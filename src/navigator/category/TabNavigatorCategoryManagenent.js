import React from 'react';
import PropTypes from 'prop-types';
import {BottomNavigation, Text} from 'react-native-paper';
import {StyleSheet} from 'react-native';
import SectionGeneralCategoryScreen
  from 'screens/category/SectionGeneralCategoryScreen';
import SectionChildrenInCategoryScreen
  from 'screens/category/SectionChildrenInCategoryScreen';
import SectionAddChildCategoryScreen
  from 'screens/category/SectionAddChildCategoryScreen';
import {Icon} from 'react-native-elements';
import {setNavigationName} from 'shared/utils/navigation';

const GENERAL_CHANGE = 'generalChange';
const СHILD_CHANGE = 'childChange';
const ADD_CHILD = 'addChild';

class CategoryManagenentNavigator extends React.Component {
  static propTypes = {
    navigation: PropTypes.object.isRequired,
  };

  static childContextTypes = {
    navigation: PropTypes.func,
  };

  getChildContext () {
    return {navigation: this.props.navigation.navigate};
  }

  state = {
    index: 0,
    routes: [{key: GENERAL_CHANGE}, {key: СHILD_CHANGE}, {key: ADD_CHILD}],
  };

  static navigationOptions = ({navigation}) => setNavigationName (navigation);

  handleIndexChange = index => this.setState ({index});

  renderScene = BottomNavigation.SceneMap ({
    [GENERAL_CHANGE]: SectionGeneralCategoryScreen,
    [СHILD_CHANGE]: SectionChildrenInCategoryScreen,
    [ADD_CHILD]: SectionAddChildCategoryScreen,
  });

  renderIcon = ({route, focused}) => {
    let color = 'grey';

    if (focused) color = '#ffffff';

    if (route.key === GENERAL_CHANGE)
      return (
        <Icon
          name="confirmation-number"
          type="confirmation_number"
          color={color}
        />
      );

    if (route.key === СHILD_CHANGE)
      return <Icon name="child-friendly" type="child-friendly" color={color} />;

    if (route.key === ADD_CHILD)
      return <Icon name="note-add" type="note-add" color={color} />;
  };

  renderLabel = ({route, focused}) => {
    if (route.key === GENERAL_CHANGE && focused)
      return <Text style={styles.label}>general Change</Text>;

    if (route.key === СHILD_CHANGE && focused)
      return <Text style={styles.label}>child Change</Text>;

    if (route.key === ADD_CHILD && focused)
      return <Text style={styles.label}>add Child</Text>;
  };

  render () {
    return (
      <BottomNavigation
        navigationState={this.state}
        onIndexChange={this.handleIndexChange}
        renderScene={this.renderScene}
        renderIcon={this.renderIcon}
        renderLabel={this.renderLabel}
        barStyle={styles.bar}
      />
    );
  }
}

const styles = StyleSheet.create ({
  bar: {
    backgroundColor: '#841584',
  },
  label: {
    textAlign: 'center',
    color: '#ffffff',
  },
});
export default CategoryManagenentNavigator;
