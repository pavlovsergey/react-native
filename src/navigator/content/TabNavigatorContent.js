import React from 'react';
import PropTypes from 'prop-types';
import {BottomNavigation, Text} from 'react-native-paper';
import {StyleSheet} from 'react-native';
import ArticleScreen from 'screens/content/ArticleScreen';
import RecipeScreen from 'screens/content/RecipeScreen';
import {setNavigationName} from 'shared/utils/navigation';
import {Icon} from 'react-native-elements';

const ARTICLE = 'ARTICLE';
const RECIPE = 'RECIPE';

class ContentNavigator extends React.Component {
  static propTypes = {
    navigation: PropTypes.object.isRequired,
  };

  static childContextTypes = {
    navigation: PropTypes.func,
  };

  getChildContext () {
    return {navigation: this.props.navigation.navigate};
  }

  state = {
    index: 0,
    routes: [{key: RECIPE}, {key: ARTICLE}],
  };

  static navigationOptions = ({navigation}) => setNavigationName (navigation);

  handleIndexChange = index => this.setState ({index});

  renderScene = BottomNavigation.SceneMap ({
    [RECIPE]: RecipeScreen,
    [ARTICLE]: ArticleScreen,
  });

  renderIcon = ({route, focused}) => {
    let color = 'grey';
    if (focused) color = '#ffffff';

    if (route.key === RECIPE) {
      return <Icon name="receipt" type="receipt" color={color} />;
    }

    if (route.key === ARTICLE) {
      return <Icon name="subject" type="subject" color={color} />;
    }
  };

  renderLabel = ({route, focused}) => {
    if (route.key === RECIPE && focused)
      return <Text style={styles.label}>Recipe</Text>;

    if (route.key === ARTICLE && focused)
      return <Text style={styles.label}>Article</Text>;
  };

  render () {
    return (
      <BottomNavigation
        navigationState={this.state}
        onIndexChange={this.handleIndexChange}
        renderScene={this.renderScene}
        renderIcon={this.renderIcon}
        renderLabel={this.renderLabel}
        barStyle={styles.bar}
      />
    );
  }
}

const styles = StyleSheet.create ({
  bar: {
    backgroundColor: '#841584',
  },
  label: {
    textAlign: 'center',
    color: '#ffffff',
  },
});
export default ContentNavigator;
