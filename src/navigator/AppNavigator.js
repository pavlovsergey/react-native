import {createStackNavigator} from 'react-navigation';
import Auth from 'screens/Auth';
import DashboardAdmin from 'screens/DashboardAdmin';
import CategoryManagement from 'screens/category/CategoryManagement';
import ContentNavigator from './content/TabNavigatorContent';
import CategoryManagenentNavigator
  from './category/TabNavigatorCategoryManagenent';
import ChangeRecipeScreen
  from 'screens/content/RecipeScreen/ChangeRecipeScreen';
import SingleRecipeScreen
  from 'screens/content/RecipeScreen/SingleRecipeScreen';

import ChangeArticleScreen
  from 'screens/content/ArticleScreen/ChangeArticleScreen';
import SingleArticleScreen
  from 'screens/content/ArticleScreen/SingleArticleScreen';

export const CATEGORY_MANAGEMENT = 'categoryManagement';
export const CONTENT_MANAGEMENT = 'contentManagement';
export const DASHBOARD_ADMIN = 'dashboardAdmin';
export const DASHBOARD_USER = 'dashboardUser';
export const CHANGE_CATEGORY = 'changeCategory';
export const CHANGE_RECIPE = 'changeRecipe';
export const SINGLE_RECIPE = 'SingleRecipe';
export const CHANGE_ARTICLE = 'changeArticle';
export const SINGLE_ARTICLE = 'SingleArticle';

const AppNavigator = createStackNavigator ({
  auth: {
    screen: Auth,
  },
  [DASHBOARD_ADMIN]: {
    screen: DashboardAdmin,
  },
  [DASHBOARD_USER]: {
    screen: ContentNavigator,
  },
  [CATEGORY_MANAGEMENT]: {
    screen: CategoryManagement,
  },
  [CONTENT_MANAGEMENT]: {
    screen: ContentNavigator,
  },
  [CHANGE_CATEGORY]: {
    screen: CategoryManagenentNavigator,
  },
  [CHANGE_RECIPE]: {
    screen: ChangeRecipeScreen,
  },
  [SINGLE_RECIPE]: {
    screen: SingleRecipeScreen,
  },
  [CHANGE_ARTICLE]: {
    screen: ChangeArticleScreen,
  },
  [SINGLE_ARTICLE]: {
    screen: SingleArticleScreen,
  },
});

export default AppNavigator;
