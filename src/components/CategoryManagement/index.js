import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {View, StyleSheet, TouchableOpacity, Text} from 'react-native';
import CategoryList from 'shared/component/CategoryList';
import {NEW_CATEGORY} from 'shared/utils/category/CRUD-categoryTree';
import {CHANGE_CATEGORY} from 'navigator/AppNavigator';
import {changeChooseCategory} from 'store/actions/category';
import {connect} from 'react-redux';

const mapDispatchToProps = {
  changeChooseCategory,
};

@connect (null, mapDispatchToProps)
export default class CategoryManagement extends Component {
  static contextTypes = {
    navigation: PropTypes.func,
  };

  handlerAddCategory = () => {
    const {changeChooseCategory} = this.props;
    const {navigation} = this.context;
    changeChooseCategory (NEW_CATEGORY);

    navigation (CHANGE_CATEGORY, {
      id: NEW_CATEGORY,
      title: 'Create New Category',
    });
  };

  static propTypes = {
    /**
     *  from connect
     */
    changeChooseCategory: PropTypes.func.isRequired,
  };

  render () {
    return (
      <View style={styles.container}>
        <View style={styles.list}>
          <CategoryList isLocal={false} isEdit={true} />
        </View>
        <View>
          <TouchableOpacity onPress={this.handlerAddCategory}>
            <Text style={styles.buttonAddCategory}>AddCategory</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create ({
  container: {
    flex: 1,
    justifyContent: 'space-between',
  },
  list: {
    height: '99%',
    marginBottom: -40,
    paddingBottom: 45,
  },
  buttonAddCategory: {
    color: '#ffffff',
    backgroundColor: '#841584',
    textAlign: 'center',
    padding: 4,
    fontSize: 22,
  },
});
