import React, {Component} from 'react';
import PropTypes from 'prop-types';
import CategoryList from 'shared/component/CategoryList/';
import {connect} from 'react-redux';
import {
  getDataForChooseParentId,
} from 'shared/utils/category/CRUD-categoryTree';

const mapStateToProps = state => {
  return {
    categoryСhildren: state.category.categoryСhildren,
    category: state.category.category,
    chooseCategory: state.category.chooseCategory,
    openCategory: state.category.openCategory,
  };
};

@connect (mapStateToProps, mapDispatchToProps)
export default class ParentCategory extends Component {
  static contextTypes = {
    navigation: PropTypes.func,
  };

  static propTypes = {
    handlerChangeParentId: PropTypes.func.isRequired,
    parentId: PropTypes.string.isRequired,
    /**
     *  from connect
     */
    categoryСhildren: PropTypes.object.isRequired,
    category: PropTypes.object.isRequired,
    chooseCategory: PropTypes.string,
    openCategory: PropTypes.array.isRequired,
  };

  constructor (props) {
    super (props);
    const {category, chooseCategory, openCategory, categoryСhildren} = props;

    this.state = getDataForChooseParentId (
      {
        category: {...category},
        openCategory: [...openCategory],
        categoryСhildren: {...categoryСhildren},
      },
      chooseCategory
    );
  }

  render () {
    const {navigation} = this.context;
    const {handlerChangeParentId, parentId} = this.props;
    const {categoryСhildren, category, openCategory} = this.state;
    return (
      <CategoryList
        isModal={true}
        isLocal={true}
        isChooseIcon={true}
        isRootCategory={true}
        customChooseCategory={handlerChangeParentId}
        categoryСhildren={categoryСhildren}
        category={category}
        chooseCategory={parentId}
        openCategory={openCategory}
        navigation={navigation}
      />
    );
  }
}
