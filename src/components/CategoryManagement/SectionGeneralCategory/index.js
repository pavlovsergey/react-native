import React, {Component} from 'react';
import PropTypes from 'prop-types';
import SaveButton from 'shared/component/SaveButton';
import {View, StyleSheet, TextInput} from 'react-native';

import Row from 'shared/component/Row';
import ParentCategory from './ParentCategory';
import {addCategory, updateCategory} from 'store/actions/category';
import {connect} from 'react-redux';

import {
  ROOT_CATEGORY,
  NEW_CATEGORY,
  getDataForChooseParentCategory,
} from 'shared/utils/category/CRUD-categoryTree';
import {notification} from 'store/actions/notification';
import {CATEGORY_MANAGEMENT} from 'navigator/AppNavigator';

const mapStateToProps = state => {
  return {
    category: state.category.category,
    chooseCategory: state.category.chooseCategory,
  };
};
const mapDispatchToProps = {addCategory, notification, updateCategory};

@connect (mapStateToProps, mapDispatchToProps)
export default class SectionGeneralCategory extends Component {
  static contextTypes = {
    navigation: PropTypes.func,
  };

  static propTypes = {
    //from connect
    category: PropTypes.object.isRequired,
    chooseCategory: PropTypes.string,
  };

  constructor (props) {
    super (props);
    const {category, chooseCategory} = props;

    this.state = getDataForChooseParentCategory (category, chooseCategory);
  }

  handlerChangeName = valueName => {
    this.setState ({valueName});
  };

  handlerChangeParentId = parentId => {
    this.setState ({parentId});
  };

  save = () => {
    const {valueName, parentId} = this.state;
    const {notification, chooseCategory, category} = this.props;

    if (!valueName) return notification ('Please enter name category');
    if (valueName === ROOT_CATEGORY)
      return notification ('Please enter different  name category');

    if (chooseCategory === NEW_CATEGORY) {
      return this.addNewCategory (valueName, parentId);
    }

    const data = category[chooseCategory];
    if (valueName !== data.title || parentId !== data.parentId) {
      this.updateTheCategory (valueName, parentId);
    }
  };

  addNewCategory = (valueName, parentId) => {
    const {addCategory} = this.props;
    addCategory (valueName, parentId);
    this.goToBack ();
  };

  updateTheCategory = (valueName, parentId) => {
    const {chooseCategory, updateCategory} = this.props;
    updateCategory (chooseCategory, valueName, parentId);
    this.goToBack ();
  };

  goToBack = () => {
    const {navigation} = this.context;
    navigation (CATEGORY_MANAGEMENT);
  };

  render () {
    const {parentId, valueName} = this.state;
    return (
      <View style={styles.container}>
        <View style={styles.content}>
          <Row title="Name">
            <TextInput
              style={styles.textInput}
              onChangeText={this.handlerChangeName}
              value={valueName}
            />
          </Row>

          <Row title="Parent Category">
            <View style={styles.item}>
              <ParentCategory
                parentId={parentId}
                handlerChangeParentId={this.handlerChangeParentId}
              />
            </View>
          </Row>
        </View>

        <View style={styles.btn}>
          <SaveButton onPress={this.save} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create ({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  content: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
  },
  btn: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  textInput: {
    flex: 1,
    fontSize: 18,
    paddingBottom: 8,
  },
  item: {
    flex: 1,
  },
});
