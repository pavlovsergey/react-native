import React, {Component} from 'react';
import {View, StyleSheet, Text} from 'react-native';

export default class SectionChildrenInCategory extends Component {
  render () {
    return (
      <View style={styles.container}>
        <Text style={styles.text}>

          Screen for future development. Now you can move the baby category:
        </Text>
        <Text style={styles.text}>1.select child category</Text>
        <Text style={styles.text}>2.change her parent</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create ({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 24,
  },
  text: {
    fontSize: 24,
    color: '#818387',
    textAlign: 'center',
  },
});
