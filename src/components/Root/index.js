import React from 'react';
import AppNavigator from 'navigator/AppNavigator';
import {View, StyleSheet} from 'react-native';
import Notification from './Notification';

const Root = () => {
  return (
    <View style={styles.container}>
      <AppNavigator />
      <Notification />
    </View>
  );
};

export default Root;

const styles = StyleSheet.create ({
  container: {
    flex: 1,
  },
});
