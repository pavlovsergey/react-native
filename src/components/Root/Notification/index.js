import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {showMessage} from 'react-native-flash-message';
import {clearNotification} from 'store/actions/notification';
import {connect} from 'react-redux';
import {View} from 'react-native';

const mapStateToProps = state => {
  return {
    isNotification: state.notification.isNotification,
    notificationMessage: state.notification.notificationMessage,
    typeNotification: state.notification.typeNotification,
    description: state.notification.description,
  };
};

const mapDispatchToProps = {clearNotification};

@connect (mapStateToProps, mapDispatchToProps)
export default class Notification extends Component {
  static propTypes = {
    isNotification: PropTypes.bool.isRequired,
    notificationMessage: PropTypes.string,
    typeNotification: PropTypes.string,
    description: PropTypes.string,
  };

  componentDidUpdate () {
    const {
      isNotification,
      notificationMessage,
      typeNotification,
      clearNotification,
      description,
    } = this.props;

    if (isNotification) {
      showMessage ({
        message: notificationMessage,
        type: typeNotification,
        description: description,
        duration: 3000,
        autoHide: typeNotification === 'danger' ? false : true,
      });
      clearNotification ();
    }
  }

  render () {
    return <View />;
  }
}
