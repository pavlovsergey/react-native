import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {View, StyleSheet} from 'react-native';
import MyButton from 'shared/component/myButton';
import {CATEGORY_MANAGEMENT, CONTENT_MANAGEMENT} from 'navigator/AppNavigator';
import {connect} from 'react-redux';

mapStateToProps = state => {
  return {
    category: state.category.category,
  };
};

@connect (mapStateToProps)
export default class DashboardAdmin extends Component {
  static propTypes = {
    navigation: PropTypes.object.isRequired,
    category: PropTypes.object.isRequired,
  };

  handlerOnPressButton = id => {
    switch (id) {
      case CATEGORY_MANAGEMENT:
        this.props.navigation.navigate (CATEGORY_MANAGEMENT);
        break;
      case CONTENT_MANAGEMENT:
        this.props.navigation.navigate (CONTENT_MANAGEMENT, {
          title: 'Content management',
        });
        break;
    }
  };

  render () {
    const {category} = this.props;
    const disabled = Object.keys (category).length < 2 ? true : false;
    return (
      <View style={styles.container}>
        <View style={styles.btn}>
          <MyButton
            handlerOnPressButton={this.handlerOnPressButton}
            id={CATEGORY_MANAGEMENT}
            name="category management"
          />
        </View>
        <View style={styles.btn}>
          <MyButton
            handlerOnPressButton={this.handlerOnPressButton}
            id={CONTENT_MANAGEMENT}
            name="content management"
            disabled={disabled}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create ({
  container: {
    flex: 1,
  },
  btn: {
    marginBottom: 12,
    marginTop: 12,
  },
});
