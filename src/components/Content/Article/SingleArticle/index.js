import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {View, StyleSheet} from 'react-native';
import Viewer from 'shared/component/Viewer';
import {connect} from 'react-redux';
import {getArticle} from 'store/selectors/articles';

const mapStateToProps = state => {
  return {
    article: getArticle (state),
  };
};

@connect (mapStateToProps)
export default class SingleArticle extends Component {
  static propTypes = {
    /**
     *  from connect
     */
    article: PropTypes.object.isRequired,
  };
  render () {
    const {article: {text}} = this.props;

    return (
      <View style={styles.container}>
        <Viewer content={JSON.parse (text)} />
      </View>
    );
  }
}

const styles = StyleSheet.create ({
  container: {
    flex: 1,
  },
  text: {
    fontSize: 24,
    color: '#818387',
    textAlign: 'center',
  },
});
