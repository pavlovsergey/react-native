import React, {Component} from 'react';
import PropTypes from 'prop-types';
import SaveButton from 'shared/component/SaveButton';
import {View, StyleSheet, TextInput} from 'react-native';
import Row from 'shared/component/Row';
import Viewer from 'shared/component/Viewer';
import Editor from 'shared/component/Editor';
import CategoryListContainer from './CategoryListContainer';
import {addArticle, updateArticle} from 'store/actions/articles';
import {connect} from 'react-redux';
import {notification} from 'store/actions/notification';
import {CONTENT_MANAGEMENT} from 'navigator/AppNavigator';
import {NEW_ARTICLE} from 'store/constans/articles';
import {getArticle} from 'store/selectors/articles';

const mapStateToProps = state => {
  return {
    category: state.category.category,
    chooseCategory: state.category.chooseCategory,
    openArticle: state.articles.openArticle,
    articlesDataByCategory: state.articles.articlesDataByCategory,
    article: getArticle (state),
  };
};

const mapDispatchToProps = {notification, addArticle, updateArticle};

@connect (mapStateToProps, mapDispatchToProps)
export default class ChangeRecipe extends Component {
  static contextTypes = {
    navigation: PropTypes.func,
  };
  constructor (props) {
    super (props);
    const {chooseCategory, openArticle, article} = props;
    this.state = {
      categoryId: chooseCategory,
      nameArticle: (openArticle && this.getNameArticle ()) || NEW_ARTICLE,
      content: JSON.parse (article.text || '{}'),
      description: article.description || '',
    };
  }
  getNameArticle = () => {
    const {chooseCategory, openArticle, articlesDataByCategory} = this.props;
    const result = articlesDataByCategory[chooseCategory].filter (article => {
      if (openArticle === article._id) {
        return true;
      }
      return false;
    });
    if (result.length > 0) return result[0].title;
    return false;
  };
  handlerChangeName = nameArticle => {
    this.setState ({nameArticle});
  };
  handlerChangeDescription = description => {
    this.setState ({description});
  };

  handlerChangeCategoryId = categoryId => {
    this.setState ({categoryId});
  };

  save = () => {
    const {nameArticle, categoryId, content, description} = this.state;
    const {
      notification,
      openArticle,
      updateArticle,
      chooseCategory,
    } = this.props;

    const newContent = JSON.stringify (content);
    if (!nameArticle) return notification ('Please enter name article');

    if (newContent === '{}') {
      return notification ('Please enter or save content article');
    }
    if (!description) return notification ('Please enter article description');

    if (!openArticle) {
      return this.addNewArticle (
        nameArticle,
        categoryId,
        newContent,
        description
      );
    }
    updateArticle (
      openArticle,
      nameArticle,
      categoryId,
      newContent,
      chooseCategory,
      description
    );
    this.goToBack ();
  };

  addNewArticle = (valueName, categoryId, content, description) => {
    const {addArticle} = this.props;
    addArticle (valueName, content, categoryId, description);
    this.goToBack ();
  };

  goToBack = () => {
    const {navigation} = this.context;
    navigation (CONTENT_MANAGEMENT);
  };

  setDataEditor = content => {
    this.setState ({content});
  };
  render () {
    const {categoryId, nameArticle, content, description} = this.state;
    return (
      <View style={styles.container}>
        <View style={styles.content}>
          <Row title="Name Article">
            <TextInput
              style={styles.textInput}
              onChangeText={this.handlerChangeName}
              value={nameArticle}
            />
          </Row>
          <Row title="Description Article">
            <TextInput
              style={styles.textInput}
              onChangeText={this.handlerChangeDescription}
              value={description}
            />
          </Row>
          <Viewer header="Body Article" content={content} />
          <View style={styles.buttonChangeContent}>
            <Editor
              titleButtonModal="change content"
              setDataEditor={this.setDataEditor}
              content={content}
            />
          </View>
          <Row title="Parent Category">
            <CategoryListContainer
              categoryId={categoryId}
              handlerChangeParentId={this.handlerChangeCategoryId}
            />
          </Row>
        </View>

        <View style={styles.btn}>
          <SaveButton onPress={this.save} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create ({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  content: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
  },
  btn: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },

  textInput: {
    flex: 1,
    fontSize: 18,
    paddingBottom: 8,
  },
  buttonChangeContent: {
    height: 40,
    alignItems: 'center',
    marginBottom: 5,
  },
});
