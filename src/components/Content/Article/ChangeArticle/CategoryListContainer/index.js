import React, {Component} from 'react';
import PropTypes from 'prop-types';
import CategoryList from 'shared/component/CategoryList/';
import {View, StyleSheet} from 'react-native';
import {connect} from 'react-redux';

const mapStateToProps = state => {
  return {
    categoryСhildren: state.category.categoryСhildren,
    category: state.category.category,
    openCategory: state.category.openCategory,
  };
};

@connect (mapStateToProps, mapDispatchToProps)
export default class ParentCategory extends Component {
  static contextTypes = {
    navigation: PropTypes.func,
  };

  static propTypes = {
    handlerChangeParentId: PropTypes.func.isRequired,
    categoryId: PropTypes.string.isRequired,
    /**
     * from connect
     */
    categoryСhildren: PropTypes.object.isRequired,
    category: PropTypes.object.isRequired,
    openCategory: PropTypes.array.isRequired,
  };

  render () {
    const {navigation} = this.context;
    const {
      handlerChangeParentId,
      categoryId,
      categoryСhildren,
      category,
      openCategory,
    } = this.props;

    return (
      <View style={styles.container}>
        <CategoryList
          isModal={true}
          isLocal={true}
          isChooseIcon={true}
          isRootCategory={false}
          customChooseCategory={handlerChangeParentId}
          categoryСhildren={categoryСhildren}
          category={category}
          chooseCategory={categoryId}
          openCategory={openCategory}
          navigation={navigation}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create ({
  container: {
    flex: 1,
  },
});
