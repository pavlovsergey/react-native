import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {View, StyleSheet} from 'react-native';
import ContentHeader from 'shared/component/ContentHeader';
import CardList from 'shared/component/CardList';
import {loader} from 'shared/component/HOC/loader';

import {connect} from 'react-redux';
import {loadArticles, openArticle, deleteArticle} from 'store/actions/articles';

import {CHANGE_ARTICLE, SINGLE_ARTICLE} from 'navigator/AppNavigator';
import {getArticlesFromCategory} from 'store/selectors/articles';

const articlesDataforLoad = state => {
  return {
    communication: state.communication.articles,
    param: {id: state.category.chooseCategory},
  };
};

const articleLoadFunc = {
  load: loadArticles,
};

const mapStateToProps = state => {
  return {
    chooseCategory: state.category.chooseCategory,
    articles: getArticlesFromCategory (state),
    openArticleСontent: state.articles.openArticle,
  };
};

const mapDispatchToProps = {
  openArticle,
  deleteArticle,
};

@connect (mapStateToProps, mapDispatchToProps)
@loader (articlesDataforLoad, articleLoadFunc)
export default class Article extends Component {
  static contextTypes = {
    navigation: PropTypes.func,
  };
  static propTypes = {
    /**
     *  from connect
     */
    chooseCategory: PropTypes.string.isRequired,
    articles: PropTypes.array.isRequired,
    deleteArticle: PropTypes.func.isRequired,
    openArticleСontent: PropTypes.string,
  };

  handleOpenArticle = (id, title) => {
    const {navigation} = this.context;
    const {openArticle} = this.props;
    openArticle (id);
    navigation (SINGLE_ARTICLE, {title});
  };

  handlerAddArticle = () => {
    const {navigation} = this.context;
    const {openArticle} = this.props;
    openArticle (null);
    navigation (CHANGE_ARTICLE, {
      title: 'Create New Article',
    });
  };

  handlerChangeArticle = (id, title) => {
    const {navigation} = this.context;
    const {openArticle} = this.props;
    openArticle (id);
    navigation (CHANGE_ARTICLE, {
      title,
    });
  };

  handlerDeleteArticle = (id, title) => {
    const {deleteArticle, chooseCategory} = this.props;
    deleteArticle (title, id, chooseCategory);
  };

  render () {
    const {articles} = this.props;
    return (
      <View style={styles.container}>
        <ContentHeader handlerAdd={this.handlerAddArticle} />
        <CardList
          dataCardList={articles || []}
          deleteFunc={this.handlerDeleteArticle}
          handleOpen={this.handleOpenArticle}
          changeFunc={this.handlerChangeArticle}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create ({
  container: {
    flex: 1,
  },
});
