import React, {Component} from 'react';
import PropTypes from 'prop-types';
import SaveButton from 'shared/component/SaveButton';
import {View, StyleSheet, TextInput} from 'react-native';
import Row from 'shared/component/Row';
import Viewer from 'shared/component/Viewer';
import Editor from 'shared/component/Editor';
import CategoryListContainer from './CategoryListContainer';
import {addRecipe, updateRecipe} from 'store/actions/recipe';
import {connect} from 'react-redux';
import {notification} from 'store/actions/notification';
import {CONTENT_MANAGEMENT} from 'navigator/AppNavigator';
import {NEW_RECIPE} from 'store/constans/recipe';
import {getRecipe} from 'store/selectors/recipes';

const mapStateToProps = state => {
  return {
    category: state.category.category,
    chooseCategory: state.category.chooseCategory,
    openRecipe: state.recipes.openRecipe,
    recipeDataByCategory: state.recipes.recipeDataByCategory,
    recipe: getRecipe (state),
  };
};

const mapDispatchToProps = {notification, addRecipe, updateRecipe};

@connect (mapStateToProps, mapDispatchToProps)
export default class ChangeRecipe extends Component {
  static contextTypes = {
    navigation: PropTypes.func,
  };
  constructor (props) {
    super (props);
    const {chooseCategory, openRecipe, recipe} = props;

    this.state = {
      categoryId: chooseCategory,
      nameRecipe: (openRecipe && recipe.title) || NEW_RECIPE,
      content: JSON.parse (recipe.text || '{}'),
    };
  }

  handlerChangeName = nameRecipe => {
    this.setState ({nameRecipe});
  };

  handlerChangeCategoryId = categoryId => {
    this.setState ({categoryId});
  };

  save = () => {
    const {nameRecipe, categoryId, content} = this.state;
    const {notification, openRecipe, updateRecipe, chooseCategory} = this.props;
    const newContent = JSON.stringify (content);
    if (!nameRecipe) return notification ('Please enter name recipe');

    if (newContent === '{}') {
      return notification ('Please enter or save content recipe');
    }

    if (!openRecipe) {
      return this.addNewRecipe (nameRecipe, categoryId, newContent);
    }
    updateRecipe (
      openRecipe,
      nameRecipe,
      categoryId,
      JSON.stringify (content),
      chooseCategory
    );
    this.goToBack ();
  };

  addNewRecipe = (valueName, categoryId, content) => {
    const {addRecipe} = this.props;
    addRecipe (valueName, content, categoryId);
    this.goToBack ();
  };

  goToBack = () => {
    const {navigation} = this.context;
    navigation (CONTENT_MANAGEMENT);
  };

  setDataEditor = content => {
    this.setState ({content});
  };
  render () {
    const {categoryId, nameRecipe, content} = this.state;
    return (
      <View style={styles.container}>
        <View style={styles.content}>
          <Row title="Name Recipe">
            <TextInput
              style={styles.textInput}
              onChangeText={this.handlerChangeName}
              value={nameRecipe}
            />
          </Row>
          <Viewer header="Body Recipe" content={content} />
          <View style={styles.buttonChangeContent}>
            <Editor
              titleButtonModal="change content"
              setDataEditor={this.setDataEditor}
              content={content}
            />
          </View>
          <Row title="Parent Category">
            <CategoryListContainer
              categoryId={categoryId}
              handlerChangeParentId={this.handlerChangeCategoryId}
            />
          </Row>
        </View>

        <View style={styles.btn}>
          <SaveButton onPress={this.save} />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create ({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  content: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
  },
  btn: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  textInput: {
    flex: 1,
    fontSize: 18,
    paddingBottom: 8,
  },
  buttonChangeContent: {
    height: 40,
    alignItems: 'center',
    marginBottom: 5,
  },
});
