import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {View, StyleSheet} from 'react-native';
import Viewer from 'shared/component/Viewer';
import {connect} from 'react-redux';
import {getRecipe} from 'store/selectors/recipes';

const mapStateToProps = state => {
  return {
    recipe: getRecipe (state),
  };
};

@connect (mapStateToProps)
export default class SingleRecipe extends Component {
  static propTypes = {
    /**
     *  from connect
     */
    recipe: PropTypes.object.isRequired,
  };
  render () {
    const {recipe: {text}} = this.props;
    return (
      <View style={styles.container}>
        <Viewer content={JSON.parse (text)} />
      </View>
    );
  }
}

const styles = StyleSheet.create ({
  container: {
    flex: 1,
  },
  text: {
    fontSize: 24,
    color: '#818387',
    textAlign: 'center',
  },
});
