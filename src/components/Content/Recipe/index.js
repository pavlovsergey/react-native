import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {View, StyleSheet} from 'react-native';
import ContentHeader from 'shared/component/ContentHeader';
import CardList from 'shared/component/CardList';
import {loader} from 'shared/component/HOC/loader';

import {connect} from 'react-redux';
import {loadRecipe, openRecipe, deleteRecipe} from 'store/actions/recipe';

import {CHANGE_RECIPE, SINGLE_RECIPE} from 'navigator/AppNavigator';
import {getRecipesFromCategory} from 'store/selectors/recipes';

const recipeDataforLoad = state => {
  return {
    communication: state.communication.recipe,
    param: {id: state.category.chooseCategory},
  };
};

const recipeLoadFunc = {
  load: loadRecipe,
};

const mapStateToProps = state => {
  return {
    chooseCategory: state.category.chooseCategory,
    recipes: getRecipesFromCategory (state),
    openRecipeСontent: state.recipes.openRecipe,
  };
};

const mapDispatchToProps = {
  openRecipe,
  deleteRecipe,
};

@connect (mapStateToProps, mapDispatchToProps)
@loader (recipeDataforLoad, recipeLoadFunc)
export default class Recipe extends Component {
  static contextTypes = {
    navigation: PropTypes.func,
  };
  static propTypes = {
    /**
     *  from connect
     */
    chooseCategory: PropTypes.string.isRequired,
    recipes: PropTypes.array.isRequired,
    deleteRecipe: PropTypes.func.isRequired,
    openRecipeСontent: PropTypes.string,
  };

  handleOpenRecipe = (id, title) => {
    const {navigation} = this.context;
    const {openRecipe} = this.props;
    openRecipe (id);
    navigation (SINGLE_RECIPE, {title});
  };

  handlerAddRecipe = () => {
    const {navigation} = this.context;
    const {openRecipe} = this.props;
    openRecipe (null);
    navigation (CHANGE_RECIPE, {
      title: 'Create New Recipe',
    });
  };

  handlerChangeRecipe = (id, title) => {
    const {navigation} = this.context;
    const {openRecipe} = this.props;
    openRecipe (id);
    navigation (CHANGE_RECIPE, {
      title,
    });
  };

  handlerDeleteRecipe = (id, title) => {
    const {deleteRecipe, chooseCategory} = this.props;
    deleteRecipe (title, id, chooseCategory);
  };

  render () {
    const {recipes} = this.props;
    return (
      <View style={styles.container}>
        <ContentHeader handlerAdd={this.handlerAddRecipe} />
        <CardList
          dataCardList={recipes || []}
          deleteFunc={this.handlerDeleteRecipe}
          handleOpen={this.handleOpenRecipe}
          changeFunc={this.handlerChangeRecipe}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create ({
  container: {
    flex: 1,
  },
});
