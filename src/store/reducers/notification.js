import {
  CREATE_CATEGORY,
  CREATE_CATEGORY_FAIL,
  DELETE_CATEGORY,
  DELETE_CATEGORY_FAIL,
  UPDATE_CATEGORY,
  UPDATE_CATEGORY_FAIL,
} from 'store/constans/category';
import {
  CREATE_RECIPE,
  CREATE_RECIPE_FAIL,
  DELETE_RECIPE,
  DELETE_RECIPE_FAIL,
  UPDATE_RECIPE,
  UPDATE_RECIPE_FAIL,
} from 'store/constans/recipe';
import {
  CREATE_ARTICLE,
  CREATE_ARTICLE_FAIL,
  DELETE_ARTICLE,
  DELETE_ARTICLE_FAIL,
  UPDATE_ARTICLE,
  UPDATE_ARTICLE_FAIL,
} from 'store/constans/articles';
import {CLEAR_NOTIFICATION, NOTIFICATION} from 'store/constans/notification';
notification;
const stateNotificationDefault = {
  isNotification: false,
  notificationMessage: '',
  typeNotification: 'info',
  description: '',
};

const notification = (
  stateNotification = stateNotificationDefault,
  {type, payload}
) => {
  switch (type) {
    /**
     * CATEGORY
     */
    case CREATE_CATEGORY:
      return {
        isNotification: true,
        notificationMessage: `Category ${payload.category.title} has been added`,
        typeNotification: 'success',
      };

    case CREATE_CATEGORY_FAIL:
      return {
        isNotification: true,
        notificationMessage: `Failed to Add Category ${payload.title}`,
        typeNotification: 'danger',
        description: payload.message,
      };

    case DELETE_CATEGORY:
      return {
        isNotification: true,
        notificationMessage: `Category ${payload.title} has been deleted`,
        typeNotification: 'success',
      };

    case DELETE_CATEGORY_FAIL:
      return {
        isNotification: true,
        notificationMessage: `Failed to delete Category ${payload.title}`,
        typeNotification: 'danger',
        description: payload.message,
      };
    case UPDATE_CATEGORY:
      return {
        isNotification: true,
        notificationMessage: `category '${payload.category.title}' has been updated`,
        typeNotification: 'success',
      };
    case UPDATE_CATEGORY_FAIL:
      return {
        isNotification: true,
        notificationMessage: `Failed to update Category ${payload.title}`,
        typeNotification: 'danger',
        description: payload.message,
      };
    /**
     * RECIPE
     */
    case CREATE_RECIPE:
      return {
        isNotification: true,
        notificationMessage: `Recipe ${payload.newRecipe.title} has been added`,
        typeNotification: 'success',
      };
    case CREATE_RECIPE_FAIL:
      return {
        isNotification: true,
        notificationMessage: `Failed to Add Recipe  ${payload.title}`,
        typeNotification: 'danger',
        description: payload.message,
      };

    case DELETE_RECIPE:
      return {
        isNotification: true,
        notificationMessage: `Recipe ${payload.title} has been deleted`,
        typeNotification: 'success',
      };

    case DELETE_RECIPE_FAIL:
      return {
        isNotification: true,
        notificationMessage: `Failed to delete recipe ${payload.title}`,
        typeNotification: 'danger',
        description: payload.message,
      };
    case UPDATE_RECIPE:
      return {
        isNotification: true,
        notificationMessage: `recipe '${payload.title}' has been updated`,
        typeNotification: 'success',
      };
    case UPDATE_RECIPE_FAIL:
      return {
        isNotification: true,
        notificationMessage: `Failed to update Recipe ${payload.title}`,
        typeNotification: 'danger',
        description: payload.message,
      };
    /**
     * ARTICLES
     */
    case CREATE_ARTICLE:
      return {
        isNotification: true,
        notificationMessage: `Article ${payload.newArticle.title} has been added`,
        typeNotification: 'success',
      };
    case CREATE_ARTICLE_FAIL:
      return {
        isNotification: true,
        notificationMessage: `Failed to Add Article  ${payload.title}`,
        typeNotification: 'danger',
        description: payload.message,
      };

    case DELETE_ARTICLE:
      return {
        isNotification: true,
        notificationMessage: `Article ${payload.title} has been deleted`,
        typeNotification: 'success',
      };

    case DELETE_ARTICLE_FAIL:
      return {
        isNotification: true,
        notificationMessage: `Failed to delete article ${payload.title}`,
        typeNotification: 'danger',
        description: payload.message,
      };
    case UPDATE_ARTICLE:
      return {
        isNotification: true,
        notificationMessage: `Article '${payload.title}' has been updated`,
        typeNotification: 'success',
      };
    case UPDATE_ARTICLE_FAIL:
      return {
        isNotification: true,
        notificationMessage: `Failed to update Article ${payload.title}`,
        typeNotification: 'danger',
        description: payload.message,
      };
    /**
     * GENERAL
     */
    case NOTIFICATION:
      return {
        isNotification: true,
        notificationMessage: payload.text,
        typeNotification: 'info',
        description: '',
      };

    case CLEAR_NOTIFICATION:
      return {
        isNotification: false,
        notificationMessage: '',
        typeNotification: 'info',
        description: '',
      };

    default:
      return stateNotification;
  }
};

export default notification;
