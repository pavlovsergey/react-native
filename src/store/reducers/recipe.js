import {
  CREATE_RECIPE,
  OPEN_RECIPE,
  DELETE_RECIPE,
  UPDATE_RECIPE,
} from 'store/constans/recipe';

import {LOAD_RECIPE} from 'store/constans/communication';

import {
  addElementToArrayFromObject,
  deleteElementToArrayFromObject,
  updateElementToArrayFromObject,
} from 'shared/utils/general';
const stateRecipeDefault = {
  openRecipe: null,
  recipeDataByCategory: {},
};

const recipes = (stateRecipes = stateRecipeDefault, {type, payload}) => {
  switch (type) {
    case LOAD_RECIPE:
      return {
        ...stateRecipes,
        recipeDataByCategory: {
          ...stateRecipes.recipeDataByCategory,
          [payload.categoryId]: payload.data,
        },
      };

    case OPEN_RECIPE:
      return {
        ...stateRecipes,
        openRecipe: payload.id,
      };

    case CREATE_RECIPE:
      const {recipeDataByCategory} = stateRecipes;
      return {
        ...stateRecipes,
        recipeDataByCategory: addElementToArrayFromObject (
          recipeDataByCategory,
          payload.newRecipe,
          payload.newRecipe.categoryId
        ),
      };

    case DELETE_RECIPE:
      return {
        ...stateRecipes,
        recipeDataByCategory: deleteElementToArrayFromObject (
          {...stateRecipes.recipeDataByCategory},
          payload.id,
          payload.categoryId
        ),
      };
    case UPDATE_RECIPE:
      return {
        ...stateRecipes,
        recipeDataByCategory: {
          ...updateElementToArrayFromObject (
            {...stateRecipes.recipeDataByCategory},
            payload,
            payload.categoryId,
            payload.previousParent
          ),
        },
      };

    default:
      return stateRecipes;
  }
};

export default recipes;
