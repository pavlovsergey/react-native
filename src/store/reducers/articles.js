import {
  CREATE_ARTICLE,
  OPEN_ARTICLE,
  DELETE_ARTICLE,
  UPDATE_ARTICLE,
} from 'store/constans/articles';

import {LOAD_ARTICLES} from 'store/constans/communication';

import {
  addElementToArrayFromObject,
  deleteElementToArrayFromObject,
  updateElementToArrayFromObject,
} from 'shared/utils/general';

const stateArticlesDefault = {
  openArticle: null,
  articlesDataByCategory: {},
};

const articles = (stateArticles = stateArticlesDefault, {type, payload}) => {
  switch (type) {
    case LOAD_ARTICLES:
      return {
        ...stateArticles,
        articlesDataByCategory: {
          ...stateArticles.articlesDataByCategory,
          [payload.categoryId]: payload.data,
        },
      };

    case OPEN_ARTICLE:
      return {
        ...stateArticles,
        openArticle: payload.id,
      };

    case CREATE_ARTICLE:
      return {
        ...stateArticles,
        articlesDataByCategory: addElementToArrayFromObject (
          {...stateArticles.articlesDataByCategory},
          payload.newArticle,
          payload.newArticle.categoryId
        ),
      };

    case DELETE_ARTICLE:
      return {
        ...stateArticles,
        articlesDataByCategory: deleteElementToArrayFromObject (
          {...stateArticles.articlesDataByCategory},
          payload.id,
          payload.categoryId
        ),
      };
    case UPDATE_ARTICLE:
      return {
        ...stateArticles,
        articlesDataByCategory: {
          ...updateElementToArrayFromObject (
            {...stateArticles.articlesDataByCategory},
            payload,
            payload.categoryId,
            payload.previousParent
          ),
        },
      };

    default:
      return stateArticles;
  }
};

export default articles;
