import {combineReducers} from 'redux';
import category from './category';
import notification from './notification';
import communication from './communication';
import recipes from './recipe';
import articles from './articles';
import auth from './auth';

export default combineReducers ({
  category,
  notification,
  communication,
  recipes,
  articles,
  auth,
});
