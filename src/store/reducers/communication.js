import {
  LOADING_CATEGORY,
  LOAD_CATEGORY,
  LOAD_CATEGORY_FAIL,
  LOADING_RECIPE,
  LOAD_RECIPE,
  LOAD_RECIPE_FAIL,
  LOADING_ARTICLES,
  LOAD_ARTICLES,
  LOAD_ARTICLES_FAIL,
} from 'store/constans/communication';
import {CREATE_CATEGORY, DELETE_CATEGORY} from 'store/constans/category';
import {
  setLoadingTrue,
  setLoadTrue,
  setLoadFail,
} from 'shared/utils/communication';

import {arrayToMapWithDefaultValue} from 'shared/utils/general';

const defaultState = {
  isLoading: false,
  isLoad: false,
  isErrorLoad: false,
  error: {message: '', name: ''},
};

const stateCommunicationDefault = {
  category: {...defaultState, message: 'Loading Category'},
  recipe: {},
  articles: {},
};

const communication = (
  stateCommunication = stateCommunicationDefault,
  {type, payload}
) => {
  switch (type) {
    /**
     * CATEGORY
     */
    case LOADING_CATEGORY:
      return {
        ...stateCommunication,
        category: setLoadingTrue (stateCommunication.category),
      };

    case LOAD_CATEGORY:
      const {category} = stateCommunication;

      return {
        ...stateCommunication,
        category: setLoadTrue (category),

        recipe: arrayToMapWithDefaultValue (payload.data, {
          ...defaultState,
          message: 'Loading Recipe',
        }),

        articles: arrayToMapWithDefaultValue (payload.data, {
          ...defaultState,
          message: 'Loading Recipe',
        }),
      };

    case CREATE_CATEGORY:
      return {
        ...stateCommunication,

        recipe: {
          ...stateCommunication.recipe,
          [payload.category._id]: {
            ...defaultState,
            message: 'Loading Recipe',
          },
        },

        articles: {
          ...stateCommunication.articles,
          [payload.category._id]: {
            ...defaultState,
            message: 'Loading Recipe',
          },
        },
      };

    case DELETE_CATEGORY:
      const newRecipe = {...stateCommunication.recipe};
      const newArticles = {...stateCommunication.articles};
      delete newRecipe[payload.id];
      delete newArticles[payload.id];
      return {
        ...stateCommunication,
        recipe: newRecipe,
        articles: newArticles,
      };

    case LOAD_CATEGORY_FAIL:
      return {
        ...stateCommunication,
        category: setLoadFail (stateCommunication.category, payload),
      };
    /**
     * RECIPE
     */
    case LOADING_RECIPE:
      return {
        ...stateCommunication,
        recipe: setLoadingTrue (stateCommunication.recipe, payload.id),
      };

    case LOAD_RECIPE:
      return {
        ...stateCommunication,
        recipe: setLoadTrue (stateCommunication.recipe, payload.categoryId),
      };

    case LOAD_RECIPE_FAIL:
      return {
        ...stateCommunication,
        recipe: setLoadFail (stateCommunication.recipe, {...payload}),
      };
    /**
     * ARTICLES
     */
    case LOADING_ARTICLES:
      return {
        ...stateCommunication,
        articles: setLoadingTrue (stateCommunication.articles, payload.id),
      };

    case LOAD_ARTICLES:
      return {
        ...stateCommunication,
        articles: setLoadTrue (stateCommunication.articles, payload.categoryId),
      };

    case LOAD_ARTICLES_FAIL:
      return {
        ...stateCommunication,
        articles: setLoadFail (stateCommunication.articles, {...payload}),
      };

    default:
      return stateCommunication;
  }
};

export default communication;
