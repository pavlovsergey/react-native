import {AUTH} from 'store/constans/auth';

const stateAuthDefault = {
  role: '',
};

const auth = (stateAuth = stateAuthDefault, {type, payload}) => {
  switch (type) {
    case AUTH:
      return {
        ...stateAuth,
        role: payload,
      };
    default:
      return stateAuth;
  }
};

export default auth;
