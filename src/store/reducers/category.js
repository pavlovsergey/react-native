import {ROOT_CATEGORY} from 'shared/utils/category/CRUD-categoryTree';
import {
  CREATE_CATEGORY,
  OPEN_CATEGORY,
  DELETE_CATEGORY,
  UPDATE_CATEGORY,
  GO_TO_CATEGORY,
  CHOOSE_CATEGORY,
  CHOOSE_FIRST_CATEGORY,
} from 'store/constans/category';

import {LOAD_CATEGORY} from 'store/constans/communication';

import {
  createTree,
  deleteCategory,
  addCategory,
  goToCategory,
  updateCategory,
  getFirstCategory,
} from 'shared/utils/category/CRUD-categoryTree';

const stateCategoryDefault = {
  categoryСhildren: {},
  category: {},
  openCategory: [ROOT_CATEGORY],
  chooseCategory: ROOT_CATEGORY,
};

const category = (stateCategory = stateCategoryDefault, {type, payload}) => {
  const {category, categoryСhildren, openCategory} = stateCategory;

  switch (type) {
    case LOAD_CATEGORY:
      const categoryTree = createTree (payload.data);
      return {
        ...stateCategory,
        ...categoryTree,
      };

    case CREATE_CATEGORY:
      const stateCreateCategory = addCategory (
        {...category},
        {...categoryСhildren},
        payload.category
      );
      return {
        ...stateCategory,
        ...stateCreateCategory,
        chooseCategory: payload.category.parentId,
      };

    case DELETE_CATEGORY:
      const stateDeleteCategory = deleteCategory (
        {
          category: {...category},
          openCategory: [...openCategory],
          categoryСhildren: {...categoryСhildren},
        },
        payload.id,
        stateCategory.chooseCategory
      );

      return {
        ...stateCategory,
        ...stateDeleteCategory,
      };

    case UPDATE_CATEGORY:
      const stateUpdateCategory = updateCategory (
        {
          category: {...category},
          openCategory: [...openCategory],
          categoryСhildren: {...categoryСhildren},
        },
        payload.category
      );
      return {
        ...stateCategory,
        ...stateUpdateCategory,
      };

    case OPEN_CATEGORY:
      return {
        ...stateCategory,
        openCategory: [...openCategory, payload],
      };

    case GO_TO_CATEGORY:
      return {
        ...stateCategory,
        openCategory: goToCategory (openCategory, payload.id),
      };

    case CHOOSE_CATEGORY:
      return {
        ...stateCategory,
        chooseCategory: payload.id,
      };
    case CHOOSE_FIRST_CATEGORY:
      return {
        ...stateCategory,
        chooseCategory: getFirstCategory (categoryСhildren),
        openCategory: [ROOT_CATEGORY],
      };
    default:
      return stateCategory;
  }
};

export default category;
