import {createSelector} from 'reselect';
const getChooseCategory = state => state.category.chooseCategory;
const getArticles = state => state.articles.articlesDataByCategory;

export const getArticlesFromCategory = createSelector (
  getChooseCategory,
  getArticles,
  (chooseCategory, articles) => {
    if (!chooseCategory) return [];
    return articles[chooseCategory] || [];
  }
);

const getOpenArticle = state => state.articles.openArticle;

export const getArticle = createSelector (
  getChooseCategory,
  getOpenArticle,
  getArticles,
  (chooseCategory, openArticle, articles) => {
    if (!chooseCategory) return {};
    if (!openArticle) return {};
    const text = articles[chooseCategory].filter (article => {
      if (article._id === openArticle) {
        return true;
      }
      return false;
    })[0];
    return text;
  }
);
