import {createSelector} from 'reselect';
const getChooseCategory = state => state.category.chooseCategory;
const getRecipes = state => state.recipes.recipeDataByCategory;

export const getRecipesFromCategory = createSelector (
  getChooseCategory,
  getRecipes,
  (chooseCategory, recipes) => {
    if (!chooseCategory) return [];
    return recipes[chooseCategory] || [];
  }
);

const getOpenRecipe = state => state.recipes.openRecipe;

export const getRecipe = createSelector (
  getChooseCategory,
  getOpenRecipe,
  getRecipes,
  (chooseCategory, openRecipe, recipes) => {
    if (!chooseCategory) return {};
    if (!openRecipe) return {};
    const text = recipes[chooseCategory].filter (recipe => {
      if (recipe._id === openRecipe) {
        return true;
      }
      return false;
    })[0];
    return text;
  }
);
