import {
  CREATE_CATEGORY,
  CREATE_CATEGORY_FAIL,
  OPEN_CATEGORY,
  DELETE_CATEGORY,
  DELETE_CATEGORY_FAIL,
  GO_TO_CATEGORY,
  CHOOSE_CATEGORY,
  UPDATE_CATEGORY,
  UPDATE_CATEGORY_FAIL,
  CHOOSE_FIRST_CATEGORY,
} from 'store/constans/category';

import {
  LOADING_CATEGORY,
  LOAD_CATEGORY,
  LOAD_CATEGORY_FAIL,
} from 'store/constans/communication';
import {checkRootCategory} from 'shared/utils/category/CRUD-categoryTree';

import {handlerError} from './error';
import axios from 'axios';

axios.defaults.headers.common['Content-Type'] = 'application/json';

export const loadCategory = () => {
  return async dispatch => {
    try {
      dispatch ({type: LOADING_CATEGORY});

      const {data} = await axios.get (
        'https://test-task-server.herokuapp.com/api/v1/category/all'
      );

      dispatch ({
        type: LOAD_CATEGORY,
        payload: {data},
      });
    } catch (e) {
      const title = 'Sorry, there was an error loading category.';
      dispatch (handlerError (LOAD_CATEGORY_FAIL, title, e));
    }
  };
};

export const addCategory = (title, parentId) => {
  return async dispatch => {
    try {
      const {
        data,
      } = await axios.post (
        'https://test-task-server.herokuapp.com/api/v1/category/create',
        {
          title,
          parentId: checkRootCategory (parentId),
        }
      );

      dispatch ({
        type: CREATE_CATEGORY,
        payload: {
          category: data,
        },
      });
    } catch (e) {
      dispatch (handlerError (CREATE_CATEGORY_FAIL, title, e));
    }
  };
};

export const updateCategory = (id, title, parentId) => {
  return async dispatch => {
    try {
      const {
        data,
      } = await axios.put (
        'https://test-task-server.herokuapp.com/api/v1/category/update',
        {
          _id: id,
          title,
          parentId: checkRootCategory (parentId),
        }
      );

      dispatch ({
        type: UPDATE_CATEGORY,
        payload: {
          category: {...data, parentId},
        },
      });
    } catch (e) {
      dispatch (handlerError (UPDATE_CATEGORY_FAIL, title, e));
    }
  };
};

export const changeOpenCategory = id => {
  return {type: OPEN_CATEGORY, payload: id};
};

export const deleteCategory = (id, title) => {
  return async dispatch => {
    try {
      await axios.delete (
        `https://test-task-server.herokuapp.com/api/v1/category/${id}`,
        {data: {id}}
      );
      dispatch ({
        type: DELETE_CATEGORY,
        payload: {
          id,
          title,
        },
      });
    } catch (e) {
      dispatch (handlerError (DELETE_CATEGORY_FAIL, title, e));
    }
  };
};

export const goCategory = id => {
  return {
    type: GO_TO_CATEGORY,
    payload: {id},
  };
};

export const changeChooseCategory = id => {
  return {
    type: CHOOSE_CATEGORY,
    payload: {id},
  };
};

export const chooseFirstCategory = () => {
  return {
    type: CHOOSE_FIRST_CATEGORY,
  };
};
