import {
  CREATE_RECIPE,
  CREATE_RECIPE_FAIL,
  OPEN_RECIPE,
  DELETE_RECIPE,
  DELETE_RECIPE_FAIL,
  UPDATE_RECIPE,
  UPDATE_RECIPE_FAIL,
} from 'store/constans/recipe';

import {
  LOADING_RECIPE,
  LOAD_RECIPE,
  LOAD_RECIPE_FAIL,
} from 'store/constans/communication';
import {chooseFirstCategory} from './category';
import {handlerError} from './error';
import axios from 'axios';
import {checkRootCategory} from 'shared/utils/category/CRUD-categoryTree';

axios.defaults.headers.common['Content-Type'] = 'application/json';

export const loadRecipe = ({id}) => {
  return async dispatch => {
    if (!checkRootCategory (id)) {
      return dispatch (chooseFirstCategory ());
    }

    try {
      dispatch ({type: LOADING_RECIPE, payload: {id}});

      const {data} = await axios.get (
        `https://test-task-server.herokuapp.com/api/v1/recipe/byCategory/${id}`
      );

      return dispatch ({
        type: LOAD_RECIPE,
        payload: {data, categoryId: id},
      });
    } catch (e) {
      const title = 'Sorry, there was an error loading recipe.';
      dispatch (handlerError (LOAD_RECIPE_FAIL, title, e, id));
    }
  };
};

export const openRecipe = id => {
  return {
    type: OPEN_RECIPE,
    payload: {id},
  };
};

export const addRecipe = (title, text, categoryId) => {
  return async dispatch => {
    try {
      const {
        data,
      } = await axios.post (
        'https://test-task-server.herokuapp.com/api/v1/recipe/create',
        {
          title,
          text,
          categoryId,
        }
      );
      dispatch ({
        type: CREATE_RECIPE,
        payload: {
          newRecipe: data,
        },
      });
    } catch (e) {
      dispatch (handlerError (CREATE_RECIPE_FAIL, title, e));
    }
  };
};

export const deleteRecipe = (title, id, categoryId) => {
  return async dispatch => {
    try {
      await axios.delete (
        `https://test-task-server.herokuapp.com/api/v1/recipe/${id}`
      );
      dispatch ({
        type: DELETE_RECIPE,
        payload: {
          title,
          id,
          categoryId,
        },
      });
    } catch (e) {
      dispatch (handlerError (DELETE_RECIPE_FAIL, title, e));
    }
  };
};

export const updateRecipe = (id, title, categoryId, text, previousParent) => {
  return async dispatch => {
    try {
      const {
        data,
      } = await axios.put (
        `https://test-task-server.herokuapp.com/api/v1/recipe/update`,
        {
          _id: id,
          title,
          text,
          categoryId,
        }
      );
      dispatch ({
        type: UPDATE_RECIPE,
        payload: {...data, previousParent},
      });
    } catch (e) {
      dispatch (handlerError (UPDATE_RECIPE_FAIL, title, e));
    }
  };
};
