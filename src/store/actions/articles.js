import {
  CREATE_ARTICLE,
  CREATE_ARTICLE_FAIL,
  OPEN_ARTICLE,
  DELETE_ARTICLE,
  DELETE_ARTICLE_FAIL,
  UPDATE_ARTICLE,
  UPDATE_ARTICLE_FAIL,
} from 'store/constans/articles';

import {
  LOADING_ARTICLES,
  LOAD_ARTICLES,
  LOAD_ARTICLES_FAIL,
} from 'store/constans/communication';
import {chooseFirstCategory} from './category';
import {handlerError} from './error';
import axios from 'axios';
import {checkRootCategory} from 'shared/utils/category/CRUD-categoryTree';

axios.defaults.headers.common['Content-Type'] = 'application/json';

export const loadArticles = ({id}) => {
  return async dispatch => {
    if (!checkRootCategory (id)) {
      return dispatch (chooseFirstCategory ());
    }

    try {
      dispatch ({type: LOADING_ARTICLES, payload: {id}});

      const {data} = await axios.get (
        `https://test-task-server.herokuapp.com/api/v1/article/byCategory/${id}`
      );
      return dispatch ({
        type: LOAD_ARTICLES,
        payload: {data, categoryId: id},
      });
    } catch (e) {
      const title = 'Sorry, there was an error loading article.';
      dispatch (handlerError (LOAD_ARTICLES_FAIL, title, e, id));
    }
  };
};

export const openArticle = id => {
  return {
    type: OPEN_ARTICLE,
    payload: {id},
  };
};

export const addArticle = (title, text, categoryId, description) => {
  return async dispatch => {
    try {
      const {
        data,
      } = await axios.post (
        'https://test-task-server.herokuapp.com/api/v1/article/create',
        {
          title,
          text,
          categoryId,
          description,
        }
      );
      dispatch ({
        type: CREATE_ARTICLE,
        payload: {
          newArticle: data,
        },
      });
    } catch (e) {
      dispatch (handlerError (CREATE_ARTICLE_FAIL, title, e));
    }
  };
};

export const deleteArticle = (title, id, categoryId) => {
  return async dispatch => {
    try {
      await axios.delete (
        `https://test-task-server.herokuapp.com/api/v1/article/${id}`
      );
      dispatch ({
        type: DELETE_ARTICLE,
        payload: {
          title,
          id,
          categoryId,
        },
      });
    } catch (e) {
      dispatch (handlerError (DELETE_ARTICLE_FAIL, title, e));
    }
  };
};

export const updateArticle = (
  id,
  title,
  categoryId,
  text,
  previousParent,
  description
) => {
  return async dispatch => {
    try {
      const {
        data,
      } = await axios.put (
        `https://test-task-server.herokuapp.com/api/v1/article/update`,
        {
          _id: id,
          title,
          text,
          categoryId,
          description,
        }
      );
      dispatch ({
        type: UPDATE_ARTICLE,
        payload: {...data, previousParent},
      });
    } catch (e) {
      dispatch (handlerError (UPDATE_ARTICLE_FAIL, title, e));
    }
  };
};
