export const handlerError = (type, title, e, param) => {
  const serverText =
    (e.response && e.response.data && e.response.data[0].message) || '';
  return {
    type,
    payload: {
      title,
      message: `${e.message}.  ${serverText}`,
      param,
    },
  };
};
