import {CLEAR_NOTIFICATION, NOTIFICATION} from 'store/constans/notification';

export const clearNotification = () => {
  return {type: CLEAR_NOTIFICATION};
};

export const notification = text => {
  return {
    type: NOTIFICATION,
    payload: {text},
  };
};
