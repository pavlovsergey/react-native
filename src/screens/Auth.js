import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {View, StyleSheet} from 'react-native';
import MyButton from 'shared/component/myButton';
import {ADMIN, USER} from 'store/constans/auth';
import {setRole} from 'store/actions/auth';
import {connect} from 'react-redux';
import {DASHBOARD_ADMIN, DASHBOARD_USER} from 'navigator/AppNavigator';
import Communications from 'react-native-communications';
import {loadCategory} from 'store/actions/category';
import {loader} from 'shared/component/HOC/loader';

mapDispatchToProps = {setRole};

mapStateToProps = state => {
  return {
    category: state.category.category,
  };
};

const categoryDataforLoad = state => {
  return {
    communication: state.communication.category,
  };
};

const categoryLoadFunc = {
  load: loadCategory,
};
@connect (mapStateToProps, mapDispatchToProps)
@loader (categoryDataforLoad, categoryLoadFunc)
class Auth extends Component {
  state = {
    index: 0,
  };

  static propTypes = {
    navigation: PropTypes.object.isRequired,
    category: PropTypes.object.isRequired,
  };

  handlerOnPressButton = id => {
    const {navigation: {navigate}, setRole} = this.props;
    switch (id) {
      case ADMIN:
        setRole (ADMIN);
        navigate (DASHBOARD_ADMIN);
        break;
      case USER:
        setRole (USER);
        navigate (DASHBOARD_USER, {
          title: 'User Page',
        });
        break;
    }
  };

  handlerOnPressGoRepository = () => {
    Communications.web (
      'https://bitbucket.org/pavlovsergey/react-native/src/master/'
    );
  };
  handlerOnPressCall = () => {
    Communications.phonecall ('0994755000', true);
  };
  render () {
    const {category} = this.props;
    const disabled = Object.keys (category).length < 2 ? true : false;
    return (
      <View style={styles.container}>
        <View>
          <View style={styles.btn}>
            <MyButton
              handlerOnPressButton={this.handlerOnPressButton}
              id={ADMIN}
              name={ADMIN}
            />
          </View>
          <View style={styles.btn}>
            <MyButton
              disabled={disabled}
              handlerOnPressButton={this.handlerOnPressButton}
              id={USER}
              name={USER}
            />
          </View>
        </View>
        <View style={styles.buttonGroup}>
          <View style={styles.btnComunication}>
            <MyButton
              handlerOnPressButton={this.handlerOnPressGoRepository}
              id={USER}
              name="Repository"
            />
          </View>
          <View style={styles.btnComunication}>
            <MyButton
              handlerOnPressButton={this.handlerOnPressCall}
              id={USER}
              name="Call"
            />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create ({
  container: {
    flex: 1,
    justifyContent: 'space-between',
  },
  btn: {
    marginBottom: 12,
  },
  buttonGroup: {
    flexDirection: 'row',

    justifyContent: 'space-between',
  },
  btnComunication: {
    flex: 1,
  },
});

export default Auth;
