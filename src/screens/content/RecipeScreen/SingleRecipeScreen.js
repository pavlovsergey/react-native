import React, {Component} from 'react';
import PropTypes from 'prop-types';
import SingleRecipe from 'components/Content/Recipe/SingleRecipe';
import {setNavigationName} from 'shared/utils/navigation';

class SingleRecipeScreen extends Component {
  static navigationOptions = ({navigation}) => setNavigationName (navigation);

  static childContextTypes = {
    navigation: PropTypes.func,
  };
  getChildContext () {
    return {navigation: this.props.navigation.navigate};
  }
  render () {
    return <SingleRecipe />;
  }
}

export default SingleRecipeScreen;
