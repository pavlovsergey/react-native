import React, {Component} from 'react';
import PropTypes from 'prop-types';
import ChangeRecipe from 'components/Content/Recipe/ChangeRecipe';
import {setNavigationName} from 'shared/utils/navigation';

class ChangeRecipeScreen extends Component {
  static navigationOptions = ({navigation}) => setNavigationName (navigation);

  static childContextTypes = {
    navigation: PropTypes.func,
  };

  getChildContext () {
    return {navigation: this.props.navigation.navigate};
  }
  render () {
    return <ChangeRecipe />;
  }
}

export default ChangeRecipeScreen;
