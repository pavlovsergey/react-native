import React from 'react';
import Recipe from 'components/Content/Recipe';

const RecipeScreen = () => <Recipe />;

export default RecipeScreen;
