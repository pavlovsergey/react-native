import React, {Component} from 'react';
import PropTypes from 'prop-types';
import SingleArticle from 'components/Content/Article/SingleArticle';
import {setNavigationName} from 'shared/utils/navigation';

class SingleArticleScreen extends Component {
  static navigationOptions = ({navigation}) => setNavigationName (navigation);

  static childContextTypes = {
    navigation: PropTypes.func,
  };
  getChildContext () {
    return {navigation: this.props.navigation.navigate};
  }
  render () {
    return <SingleArticle />;
  }
}

export default SingleArticleScreen;
