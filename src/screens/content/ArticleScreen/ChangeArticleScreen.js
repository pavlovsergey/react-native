import React, {Component} from 'react';
import PropTypes from 'prop-types';
import ChangeArticle from 'components/Content/Article/ChangeArticle';
import {setNavigationName} from 'shared/utils/navigation';

class ChangeArticleScreen extends Component {
  static navigationOptions = ({navigation}) => setNavigationName (navigation);

  static childContextTypes = {
    navigation: PropTypes.func,
  };

  getChildContext () {
    return {navigation: this.props.navigation.navigate};
  }
  render () {
    return <ChangeArticle />;
  }
}

export default ChangeArticleScreen;
