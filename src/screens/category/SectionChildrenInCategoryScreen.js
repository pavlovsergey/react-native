import React from 'react';
import SectionChildrenInCategory
  from 'components/CategoryManagement/SectionChildrenInCategory';

const SectionChildrenInCategoryScreen = () => <SectionChildrenInCategory />;

export default SectionChildrenInCategoryScreen;
