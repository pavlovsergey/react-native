import React, {Component} from 'react';
import CategoryManagement from 'components/CategoryManagement';
import PropTypes from 'prop-types';
class CategoryManagementScreen extends Component {
  static navigationOptions = {
    title: 'Category Management',
  };

  static childContextTypes = {
    navigation: PropTypes.func,
  };
  getChildContext () {
    return {navigation: this.props.navigation.navigate};
  }

  render () {
    return <CategoryManagement />;
  }
}

export default CategoryManagementScreen;
