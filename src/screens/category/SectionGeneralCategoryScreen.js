import React from 'react';
import SectionGeneralCategory
  from 'components/CategoryManagement/SectionGeneralCategory';

const SectionGeneralCategoryScreen = () => <SectionGeneralCategory />;

export default SectionGeneralCategoryScreen;
