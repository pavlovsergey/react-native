import React from 'react';
import SectionAddChildCategory
  from 'components/CategoryManagement/SectionAddChildCategory';

const SectionAddChildCategoryScreen = () => <SectionAddChildCategory />;

export default SectionAddChildCategoryScreen;
