import React, {Component} from 'react';
import DashboardAdmin from 'components/DashboardAdmin';

export default class DashboardAdminScreen extends Component {
  static navigationOptions = {
    title: 'Dashboard Admin',
  };

  render () {
    const {navigation} = this.props;
    return <DashboardAdmin navigation={navigation} />;
  }
}
