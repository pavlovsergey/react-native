import React from 'react';
import PropTypes from 'prop-types';
import {Bubbles} from 'react-native-loader';
import {View, StyleSheet, Text} from 'react-native';

const Loader = ({label}) => {
  const labelText = label || 'Loading...';

  return (
    <View style={styles.container}>
      <Text style={styles.label}>
        {labelText}
      </Text>
      <Bubbles size={18} color="#FFF" />
    </View>
  );
};

Loader.propTypes = {
  label: PropTypes.string,
};

const styles = StyleSheet.create ({
  container: {
    flex: 1,
    backgroundColor: '#841584',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
  },
  label: {
    color: '#ffffff',
    fontSize: 24,
  },
});

export default Loader;
