import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Text, View, StyleSheet, Modal} from 'react-native';
import {Constants} from 'expo';

class Loader extends Component {
  static propTypes = {
    label: PropTypes.string,
    duration: PropTypes.number,
  };

  state = {
    modalVisible: true,
  };
  closeModal = () => {
    this.setState ({modalVisible: false});
  };

  componentDidMount () {
    const {duration} = this.props;
    if (duration) setTimeout (this.closeModal, duration);
  }
  render () {
    const labelText = this.props.label || 'Loading...';

    return (
      <View>
        <Modal
          animationType="fade"
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => this.closeModal}
        >
          <View style={styles.modalBackgroundStyle}>
            <View style={styles.innerContainerTransparentStyle}>
              <Text style={styles.label}>{labelText}</Text>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create ({
  modalBackgroundStyle: {
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    alignItems: 'center',
    justifyContent: 'center',
    alignContent: 'stretch',
    paddingTop: Constants.statusBarHeight,
  },
  innerContainerTransparentStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#841584',
    flex: 1,
    width: '100%',
  },
  label: {
    color: '#ffffff',
    fontSize: 24,
  },
});

export default Loader;
