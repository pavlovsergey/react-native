import React from 'react';
import PropTypes from 'prop-types';
import {Text, StyleSheet} from 'react-native';

const Label = ({text, customStyle}) => (
  <Text style={[styles.label, customStyle]}>{text}</Text>
);

Label.propTypes = {
  text: PropTypes.string.isRequired,
  customStyle: PropTypes.object,
};

const styles = StyleSheet.create ({
  label: {
    fontSize: 18,
    textDecorationLine: 'underline',
    fontWeight: '700',
    marginRight: 15,
  },
});

export default Label;
