import React from 'react';
import PropTypes from 'prop-types';
import {View, StyleSheet} from 'react-native';
import Label from './Label';

const Row = ({children, title, customLabelStyle}) => {
  return (
    <View style={styles.container}>
      <Label customStyle={customLabelStyle} text={`${title} :`} />
      {children}
    </View>
  );
};

Row.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.object.isRequired,
  customStyle: PropTypes.object,
};

const styles = StyleSheet.create ({
  container: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    margin: 15,
    alignItems: 'flex-start',
  },
});

export default Row;
