import React from 'react';
import PropTypes from 'prop-types';
import {Header} from 'react-native-elements';
import CategoryList from 'shared/component/CategoryList/';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import {Icon} from 'react-native-elements';
import {ADMIN} from 'store/constans/auth';
import Permission from 'shared/component/Permission';

export default function ContentHeader({handlerAdd}) {
  return (
    <Header
      centerComponent={
        <View style={styles.category}>
          <CategoryList isLocal={false} isModal={true} isChooseIcon={true} />
        </View>
      }
      rightComponent={
        <Permission role={[ADMIN]}>
          <View style={styles.addButton}>
            <TouchableOpacity onPress={handlerAdd}>
              <Icon name="note-add" type="note-add" color="#ffffff" />
            </TouchableOpacity>
          </View>
        </Permission>
      }
      backgroundColor="#841584"
    />
  );
}

const styles = StyleSheet.create ({
  addButton: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  category: {
    backgroundColor: '#ffffff',
    paddingLeft: 15,
    paddingRight: 15,
    paddingTop: 5,
    paddingBottom: 5,
    minWidth: 200,
  },
});
