import React, {Component} from 'react';
import PropTypes from 'prop-types';

import {View, StyleSheet, Text} from 'react-native';

const Error = ({error: {message, name}}) => {
  const labelText = message || name
    ? `${name}  ${message}`
    : 'Sorry an error has occurred.';

  return (
    <View style={styles.container}>
      <Text style={styles.label}>
        {labelText}
      </Text>
    </View>
  );
};

Error.propTypes = {
  error: PropTypes.object,
};

const styles = StyleSheet.create ({
  container: {
    flex: 1,
    backgroundColor: '#841584',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
  },
  label: {
    color: '#ffffff',
    fontSize: 24,
    textAlign: 'center',
  },
});

export default Error;
