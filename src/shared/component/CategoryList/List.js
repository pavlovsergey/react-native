import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {View} from 'react-native';
import BodyCategoryList from './BodyCategoryList';
import HeaderCategoryList from './HeaderCategoryList';
import modal from './Hoc/modal';

import ModalLoader from '../Loader/ModalLoader';

const LABEL_MODAL = 'Your category';

@modal
export default class List extends Component {
  static propTypes = {
    categoryСhildren: PropTypes.object.isRequired,
    category: PropTypes.object.isRequired,
    openCategory: PropTypes.array.isRequired,
    chooseCategory: PropTypes.string,
  };

  componentDidMount;
  render () {
    const {
      categoryСhildren,
      category,
      openCategory,
      chooseCategory,
    } = this.props;
    return (
      <View>
        <ModalLoader duration={1} label={LABEL_MODAL} />
        <HeaderCategoryList category={category} openCategory={openCategory} />
        <BodyCategoryList
          categoryСhildren={categoryСhildren}
          category={category}
          openCategory={openCategory}
          chooseCategory={chooseCategory}
        />
      </View>
    );
  }
}
