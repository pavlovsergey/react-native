import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {View, StyleSheet, Text, TouchableOpacity} from 'react-native';
import {Icon} from 'react-native-elements';

export default class ItemHeader extends Component {
  static contextTypes = {
    goCategory: PropTypes.func.isRequired,
  };

  static propTypes = {
    title: PropTypes.string,
    id: PropTypes.string,
    isIconTransition: PropTypes.bool,
  };

  handlerGoCategory = () => {
    const {id} = this.props;
    const {goCategory} = this.context;
    goCategory (id);
  };

  render () {
    const {title, isIconTransition} = this.props;
    return (
      <View style={styles.container}>
        <TouchableOpacity onPress={this.handlerGoCategory}>
          <Text style={styles.title}>
            {title}
          </Text>
        </TouchableOpacity>
        {isIconTransition &&
          <TouchableOpacity onPress={this.handlerGoCategory}>
            <Icon
              name="arrow-drop-up"
              type="arrow-drop-up"
              size={28}
              iconStyle={styles.icon}
            />
          </TouchableOpacity>}
      </View>
    );
  }
}

const styles = StyleSheet.create ({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: '#808080',
    alignItems: 'center',
    backgroundColor: '#c0c0c0',
    paddingTop: 2,
    paddingBottom: 2,
    paddingLeft: 15,
  },
  icon: {
    paddingLeft: 5,
    paddingRight: 5,
    color: '#404040',
  },
  title: {
    fontWeight: '600',
    fontSize: 12,
    color: '#404040',
  },
});
