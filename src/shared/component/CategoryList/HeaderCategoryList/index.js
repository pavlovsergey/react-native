import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {FlatList} from 'react-native';
import ItemHeader from './ItemHeader';

export default class HeaderCategoryList extends Component {
  static propTypes = {
    category: PropTypes.object,
    openCategory: PropTypes.array,
  };

  renderItemList = ({item}) => (
    <ItemHeader
      key={item.key}
      title={item.title}
      isIconTransition={this.props.openCategory.length > 1}
      id={item.key}
    />
  );

  getItemLayoutList = (data, index) => ({
    length: 16,
    offset: 16 * index,
    index,
  });

  getDataBodyHeader = () => {
    const {category, openCategory} = this.props;
    return openCategory.map (item => {
      return {key: item, title: category[item].title};
    });
  };

  render () {
    return (
      <FlatList
        data={this.getDataBodyHeader ()}
        renderItem={this.renderItemList}
        getItemLayout={this.getItemLayoutList}
        removeClippedSubviews={true}
        initialNumToRender={12}
        updateCellsBatchingPeriod={25}
        windowSize={10}
      />
    );
  }
}
