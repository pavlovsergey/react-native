import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import List from './List';
import {
  changeOpenCategory,
  deleteCategory,
  changeChooseCategory,
  goCategory,
} from 'store/actions/category';

import {notification} from 'store/actions/notification';

const mapStateToProps = state => {
  return {
    categoryСhildren: state.category.categoryСhildren,
    category: state.category.category,
    openCategory: state.category.openCategory,
    chooseCategory: state.category.chooseCategory,
  };
};

const mapDispatchToProps = {
  changeOpenCategory,
  deleteCategory,
  changeChooseCategory,
  goCategory,
  notification,
};

@connect (mapStateToProps, mapDispatchToProps)
export default class DataRedux extends Component {
  static propTypes = {
    isEdit: PropTypes.bool,
    isChooseIcon: PropTypes.bool,
    isRootCategory: PropTypes.bool,
    isModal: PropTypes.bool,
    /**
     * from connect
     */
    changeOpenCategory: PropTypes.func,
    changeChooseCategory: PropTypes.func,
    deleteCategory: PropTypes.func,
    notification: PropTypes.func,
    goCategory: PropTypes.func,

    categoryСhildren: PropTypes.object.isRequired,
    category: PropTypes.object.isRequired,
    openCategory: PropTypes.array.isRequired,
    chooseCategory: PropTypes.string,
  };

  static childContextTypes = {
    changeOpenCategory: PropTypes.func.isRequired,
    notification: PropTypes.func,
    changeChooseCategory: PropTypes.func,
    deleteCategory: PropTypes.func,
    handlerClickTitle: PropTypes.func,
    goCategory: PropTypes.func.isRequired,
    isEdit: PropTypes.bool,
    isChooseIcon: PropTypes.bool,
    isRootCategory: PropTypes.bool,
  };
  getChildContext () {
    const {isChooseIcon, changeChooseCategory} = this.props;
    let customChooseCategory = this.props.customChooseCategory;
    if (isChooseIcon && !customChooseCategory)
      customChooseCategory = changeChooseCategory;

    return {
      isEdit: this.props.isEdit,
      isChooseIcon,
      changeOpenCategory: this.props.changeOpenCategory,
      notification: this.props.notification,
      changeChooseCategory: this.props.changeChooseCategory,
      deleteCategory: this.props.deleteCategory,
      handlerClickTitle: customChooseCategory ? customChooseCategory : null,
      goCategory: this.props.goCategory,
      isRootCategory: this.props.isRootCategory,
    };
  }

  render () {
    const {
      categoryСhildren,
      category,
      openCategory,
      chooseCategory,
      isModal,
    } = this.props;

    return (
      <List
        isModal={isModal}
        openCategory={openCategory}
        categoryСhildren={categoryСhildren}
        category={category}
        chooseCategory={chooseCategory}
      />
    );
  }
}
