import React, {Component} from 'react';
import PropTypes from 'prop-types';
import List from './List';
import {
  deleteCategory,
  goToCategory,
} from 'shared/utils/category/CRUD-categoryTree';
import {notification} from 'store/actions/notification';

export default class Local extends Component {
  constructor (props) {
    super (props);
    const {openCategory, category, categoryСhildren, chooseCategory} = props;

    this.state = {
      category,
      openCategory,
      categoryСhildren,
      chooseCategory,
    };
  }

  static propTypes = {
    categoryСhildren: PropTypes.object.isRequired,
    category: PropTypes.object.isRequired,
    openCategory: PropTypes.array.isRequired,
    chooseCategory: PropTypes.string,

    customChangeOpenCategory: PropTypes.func,
    customChooseCategory: PropTypes.func,
    customDeleteCategory: PropTypes.func,
    customGoCategory: PropTypes.func,

    isEdit: PropTypes.bool,
    isChooseIcon: PropTypes.bool,
    isRootCategory: PropTypes.bool,
    modal: PropTypes.object,
  };

  static childContextTypes = {
    changeOpenCategory: PropTypes.func.isRequired,
    notification: PropTypes.func,
    chooseCategory: PropTypes.func,
    deleteCategory: PropTypes.func,
    handlerClickTitle: PropTypes.func,
    goCategory: PropTypes.func.isRequired,
    isEdit: PropTypes.bool,
    isChooseIcon: PropTypes.bool,
    isRootCategory: PropTypes.bool,
  };

  getChildContext () {
    return {
      changeOpenCategory: this.changeOpenCategory,
      notification,
      chooseCategory: this.chooseCategory,
      deleteCategory: this.deleteCategory,
      handlerClickTitle: this.props.customChooseCategory
        ? this.changeChooseCategory
        : null,
      goCategory: this.goCategory,
      isEdit: this.props.isEdit,
      isChooseIcon: this.props.isChooseIcon,
      isRootCategory: this.props.isRootCategory,
    };
  }

  changeOpenCategory = id => {
    const {openCategory} = this.state;
    const {customChangeOpenCategory} = this.props;
    this.setState ({
      openCategory: [...openCategory, id],
    });
    customChangeOpenCategory && customChangeOpenCategory (id);
  };

  deleteCategory = id => {
    const {categoryСhildren, openCategory} = this.state;
    const {customDeleteCategory} = this.props;
    this.setState ({
      categoryСhildren: deleteCategory (
        openCategory[openCategory.length - 1],
        categoryСhildren,
        id
      ),
    });

    customDeleteCategory && customDeleteCategory (id);
  };

  changeChooseCategory = id => {
    const {customChooseCategory} = this.props;
    this.setState ({chooseCategory: id});
    customChooseCategory && customChooseCategory (id);
  };

  goCategory = id => {
    const {openCategory} = this.state;
    const {customGoCategory} = this.props;
    this.setState ({
      openCategory: goToCategory (openCategory, id),
    });

    customGoCategory && customGoCategory (id);
  };

  render () {
    const {
      categoryСhildren,
      category,
      openCategory,
      chooseCategory,
    } = this.state;
    const {isModal} = this.props;

    return (
      <List
        isModal={isModal}
        openCategory={openCategory}
        categoryСhildren={categoryСhildren}
        category={category}
        chooseCategory={chooseCategory}
      />
    );
  }
}
