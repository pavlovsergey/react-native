import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Button,
  Modal,
  TouchableOpacity,
} from 'react-native';
import PropTypes from 'prop-types';
import {Constants} from 'expo';
import {Icon} from 'react-native-elements';

export default class DeleteModalButton extends Component {
  static propTypes = {
    message: PropTypes.string.isRequired,
    handlerDeleteCategory: PropTypes.func.isRequired,
  };

  state = {
    modalVisible: false,
  };

  closeModal = () => {
    this.setState({modalVisible: false});
  };
  openModal = () => {
    this.setState({modalVisible: true});
  };

  render() {
    const {message, handlerDeleteCategory} = this.props;
    return (
      <View style={styles.container}>
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => this.closeModal}
        >
          <View style={styles.modalBackgroundStyle}>
            <View style={styles.innerContainerTransparentStyle}>
              <Text>{message}</Text>
              <View style={styles.buttonContainer}>
                <View style={styles.button}>
                  <Button title="close" onPress={this.closeModal} />
                </View>
                <View style={styles.button}>
                  <Button
                    title="delete"
                    onPress={handlerDeleteCategory}
                    color="#841584"
                  />
                </View>
              </View>
            </View>
          </View>
        </Modal>
        <TouchableOpacity onPress={this.openModal}>
          <Icon
            name="delete-forever"
            type="delete-forever"
            size={28}
            iconStyle={styles.icon}
          />
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  modalBackgroundStyle: {
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: Constants.statusBarHeight,
  },
  innerContainerTransparentStyle: {
    backgroundColor: '#ffffff',
    padding: 20,
    width: '90%',
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    marginTop: 15,
  },
  button: {
    marginLeft: 10,
  },
});
