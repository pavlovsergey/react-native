import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import DeleteModalButton from './DeleteModalButton';
import {Icon} from 'react-native-elements';
import {CHANGE_CATEGORY} from 'navigator/AppNavigator';
export default class Edit extends Component {
  static contextTypes = {
    deleteCategory: PropTypes.func,
    isEdit: PropTypes.bool,
    navigation: PropTypes.func,
    changeChooseCategory: PropTypes.func,
  };

  handlerDeleteCategory = () => {
    const {id, title} = this.props;
    const {deleteCategory} = this.context;
    deleteCategory (id, title);
  };

  handlerChangeCategory = () => {
    const {id, title} = this.props;
    const {navigation, changeChooseCategory} = this.context;
    changeChooseCategory (id);
    navigation (CHANGE_CATEGORY, {id, title: `Change category - "${title}"`});
  };

  render () {
    const {title} = this.props;
    const {isEdit} = this.context;
    if (!isEdit) return null;
    return (
      <View style={styles.container}>
        <View style={styles.containerButton}>
          <TouchableOpacity onPress={this.handlerChangeCategory}>
            <Icon name="edit" type="edit" size={28} iconStyle={styles.icon} />
          </TouchableOpacity>
        </View>

        <View style={styles.containerButton}>
          <DeleteModalButton
            message={`You definitely want to delete the category ${title}`}
            handlerDeleteCategory={this.handlerDeleteCategory}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create ({
  container: {
    flexDirection: 'row',
  },

  containerButton: {
    width: 35,
  },

  icon: {
    paddingLeft: 5,
    paddingRight: 5,
    color: '#404040',
  },
});
