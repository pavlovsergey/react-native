import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {View, StyleSheet, Text, TouchableOpacity} from 'react-native';
import Edit from './Edit';
import {Icon} from 'react-native-elements';

export default class Category extends Component {
  static contextTypes = {
    changeOpenCategory: PropTypes.func.isRequired,
    notification: PropTypes.func,
    handlerClickTitle: PropTypes.func,
    isChooseIcon: PropTypes.bool,
    handlerCloseModal: PropTypes.func,
  };

  static propTypes = {
    title: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    isChild: PropTypes.bool.isRequired,
  };

  handlerOpenCategory = () => {
    const {id, isChild, title} = this.props;
    const {notification, changeOpenCategory} = this.context;
    if (isChild) return changeOpenCategory (id);
    notification (`The category  ${title} has no children`);
  };

  handlerClickName = () => {
    const {id, isChild, title} = this.props;

    const {handlerClickTitle, handlerCloseModal} = this.context;

    if (!handlerClickTitle) return this.handlerOpenCategory ();
    handlerClickTitle (id, isChild, title);
    return handlerCloseModal ();
  };

  getIconChild = () => {
    if (this.props.isChild)
      return (
        <TouchableOpacity onPress={this.handlerOpenCategory}>
          <Icon
            name="chevron-right"
            type="chevron-right"
            size={28}
            iconStyle={styles.icon}
          />
        </TouchableOpacity>
      );
    return <View />;
  };
  handlerUpdateCategory = () => {};

  render () {
    const {title, id, isChooseCategory} = this.props;
    const {isChooseIcon} = this.context;
    return (
      <View style={styles.container}>
        <View style={styles.containerButton}>
          {this.getIconChild ()}
        </View>
        <View style={styles.titleContainer}>

          <TouchableOpacity onPress={this.handlerClickName}>
            <Text style={styles.title}>
              {title}
            </Text>
          </TouchableOpacity>
        </View>
        {isChooseCategory &&
          isChooseIcon &&
          <Icon
            name="check"
            type="check"
            size={28}
            iconStyle={styles.activeIcon}
          />}

        <Edit title={title} id={id} />
      </View>
    );
  }
}

const styles = StyleSheet.create ({
  container: {
    flex: 1,
    flexDirection: 'row',
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: '#c0c0c0',
    alignItems: 'center',
    paddingTop: 15,
    paddingBottom: 15,
    paddingRight: 10,
  },
  containerButton: {
    width: 35,
  },
  icon: {
    paddingLeft: 5,
    paddingRight: 5,
    color: '#404040',
  },
  activeIcon: {
    color: '#ffffff',
    backgroundColor: '#32CD32',
    padding: 2,
    borderRadius: 15,
  },
  active: {
    color: '#49FF33',
  },
  titleContainer: {
    flex: 1,
    overflow: 'hidden',
  },
  title: {
    fontWeight: '600',
    fontSize: 18,
    color: '#404040',
  },
});
