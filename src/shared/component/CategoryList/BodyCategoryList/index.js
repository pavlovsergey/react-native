import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {FlatList} from 'react-native';
import Category from './Category';
import {getDataFlatList} from 'shared/utils/category/CRUD-categoryTree';

export default class BodyCategoryList extends Component {
  static contextTypes = {
    isRootCategory: PropTypes.bool,
  };
  static propTypes = {
    categoryСhildren: PropTypes.object,
    category: PropTypes.object,
    openCategory: PropTypes.array,
    chooseCategory: PropTypes.string,
  };

  renderItemList = ({item}) => {
    const {chooseCategory} = this.props;
    return (
      <Category
        key={item.key}
        title={item.title}
        isChild={item.isChild}
        id={item.key}
        isChooseCategory={chooseCategory === item.key ? true : false}
      />
    );
  };

  getItemLayoutList = (data, index) => ({
    length: 40,
    offset: this.length * index,
    index,
  });

  render () {
    const {categoryСhildren, category, openCategory} = this.props;
    const {isRootCategory} = this.context;

    this.bodyList = getDataFlatList (
      {
        category: {...category},
        categoryСhildren: {...categoryСhildren},
        openCategory: [...openCategory],
      },
      isRootCategory
    );
    return (
      <FlatList
        data={this.bodyList}
        renderItem={this.renderItemList}
        getItemLayout={this.getItemLayoutList}
        removeClippedSubviews={true}
        initialNumToRender={12}
        updateCellsBatchingPeriod={25}
        windowSize={10}
      />
    );
  }
}
