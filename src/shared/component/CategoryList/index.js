import React from 'react';
import PropTypes from 'prop-types';
import Local from './Local';
import DataRedux from './DataRedux';

export default function CategoryList (props) {
  if (props.isLocal) return <Local {...props} />;
  return <DataRedux {...props} />;
}

CategoryList.propTypes = {
  isLocal: PropTypes.bool.isRequired,
};
