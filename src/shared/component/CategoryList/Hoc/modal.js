import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {
  Text,
  View,
  StyleSheet,
  Button,
  Modal,
  TouchableOpacity,
} from 'react-native';
import {Constants} from 'expo';

export default ChildComponent =>
  class extends Component {
    state = {
      isVisibleModal: false,
    };

    static propTypes = {
      isModal: PropTypes.bool,
    };

    static childContextTypes = {
      handlerCloseModal: PropTypes.func,
    };

    getChildContext () {
      return {
        handlerCloseModal: this.handlerCloseModal,
      };
    }

    handlerOpenModal = () => {
      this.setState ({isVisibleModal: true});
    };

    handlerCloseModal = () => {
      this.setState ({isVisibleModal: false});
    };

    render () {
      if (!this.props.isModal) return <ChildComponent {...this.props} />;
      const {isVisibleModal} = this.state;
      const {chooseCategory, category} = this.props;
      const textButton =
        (chooseCategory &&
          category[chooseCategory] &&
          category[chooseCategory].title) ||
        ' ';
      return (
        <View style={styles.container}>
          <Modal
            animationType="slide"
            transparent={true}
            visible={isVisibleModal}
            onRequestClose={this.handlerCloseModal}
          >
            <View style={styles.modalBackgroundStyle}>
              <View style={styles.innerContainerTransparentStyle}>
                <View style={styles.list}>
                  <ChildComponent {...this.props} />
                </View>

                <View style={styles.buttonContainer}>
                  <View style={styles.button}>
                    <Button title="close" onPress={this.handlerCloseModal} />
                  </View>
                </View>
              </View>
            </View>
          </Modal>

          <TouchableOpacity
            onPress={this.handlerOpenModal}
            style={styles.buttonName}
          >
            <Text style={styles.textButtonName}>{textButton} </Text>
          </TouchableOpacity>
        </View>
      );
    }
  };

const styles = StyleSheet.create ({
  modalBackgroundStyle: {
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: Constants.statusBarHeight,
  },
  innerContainerTransparentStyle: {
    display: 'flex',
    justifyContent: 'space-between',
    backgroundColor: '#ffffff',
    padding: 10,
    width: '90%',
    height: '85%',
  },
  list: {height: '80%'},
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    marginTop: 15,
  },
  buttonName: {
    borderBottomWidth: 1,
    borderBottomColor: '#000000',
  },
  textButtonName: {
    fontSize: 18,
  },
  button: {
    marginLeft: 10,
  },
});
