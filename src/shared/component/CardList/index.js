import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {FlatList} from 'react-native';

import CardItem from './CardItem';

export default class CardList extends Component {
  static propTypes = {
    dataCardList: PropTypes.array.isRequired,
    deleteFunc: PropTypes.func.isRequired,
    changeFunc: PropTypes.func.isRequired,
    handleOpen: PropTypes.func.isRequired,
  };

  renderCardsList = ({item}) => {
    const {deleteFunc, handleOpen, changeFunc} = this.props;
    return (
      <CardItem
        deleteFunc={deleteFunc}
        changeFunc={changeFunc}
        handleOpen={handleOpen}
        description={item.description}
        title={item.title}
        id={item._id}
        text={JSON.parse (item.text)}
      />
    );
  };
  getItemLayoutList = (data, index) => ({
    length: 110,
    offset: this.length * index,
    index,
  });
  keyExtractor = item => item._id;
  render () {
    const {dataCardList} = this.props;
    return (
      <FlatList
        data={dataCardList}
        renderItem={this.renderCardsList}
        removeClippedSubviews={true}
        initialNumToRender={12}
        updateCellsBatchingPeriod={25}
        windowSize={10}
        keyExtractor={this.keyExtractor}
        getItemLayout={this.getItemLayoutList}
      />
    );
  }
}
