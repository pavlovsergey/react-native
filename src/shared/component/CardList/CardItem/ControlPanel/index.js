import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import {Icon} from 'react-native-elements';
import DeleteButton from 'shared/component/DeleteButton';
import Permission from 'shared/component/Permission';
import {ADMIN} from 'store/constans/auth';

export default class ControlPanel extends Component {
  static propTypes = {
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    deleteFunc: PropTypes.func.isRequired,
    changeFunc: PropTypes.func,
  };

  handlerDelete = () => {
    const {id, title, deleteFunc} = this.props;
    if (deleteFunc) deleteFunc (id, title);
  };

  handlerChange = () => {
    const {id, title, changeFunc} = this.props;
    if (changeFunc) changeFunc (id, title);
  };

  render () {
    const {title} = this.props;
    return (
      <Permission role={[ADMIN]}>
        <View style={styles.container}>
          <View style={styles.containerButton}>
            <TouchableOpacity onPress={this.handlerChange}>
              <Icon name="edit" type="edit" size={28} iconStyle={styles.icon} />
            </TouchableOpacity>
          </View>

          <View style={styles.containerButton}>
            <DeleteButton
              message={`You definitely want to delete the recipe ${title}`}
              handlerDelete={this.handlerDelete}
            />
          </View>
        </View>
      </Permission>
    );
  }
}

const styles = StyleSheet.create ({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
  },

  containerButton: {
    width: 35,
  },

  icon: {
    paddingLeft: 5,
    paddingRight: 5,
    color: '#404040',
  },
});
