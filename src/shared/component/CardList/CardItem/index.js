import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Card, Button} from 'react-native-elements';
import {View, StyleSheet, Text} from 'react-native';
import Viewer from 'shared/component/Viewer';
import ControlPanel from './ControlPanel';

export default class CardItem extends Component {
  static propTypes = {
    description: PropTypes.string,
    title: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    text: PropTypes.object.isRequired,
    deleteFunc: PropTypes.func.isRequired,
    changeFunc: PropTypes.func.isRequired,
    handleOpen: PropTypes.func.isRequired,
  };
  handleOpenClick = () => {
    const {handleOpen, id, title} = this.props;
    handleOpen (id, title);
  };
  getBody = () => {
    const {text, description} = this.props;
    if (description) return <Text>{description}</Text>;
    return (
      <View style={styles.container}>
        <Viewer content={text} />
      </View>
    );
  };
  render () {
    const {title, id, changeFunc, deleteFunc} = this.props;
    return (
      <Card title={title} key={id}>
        {this.getBody ()}
        <Button
          onPress={this.handleOpenClick}
          icon={{name: 'code'}}
          backgroundColor="#841584"
          buttonStyle={{
            borderRadius: 0,
            marginLeft: 0,
            marginRight: 0,
            marginBottom: 0,
          }}
          title="VIEW NOW"
        />
        <ControlPanel
          title={title}
          id={id}
          deleteFunc={deleteFunc}
          changeFunc={changeFunc}
        />
      </Card>
    );
  }
}

const styles = StyleSheet.create ({
  container: {
    flex: 1,
    height: 100,
  },
});
