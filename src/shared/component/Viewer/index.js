import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {View, StyleSheet, Button} from 'react-native';
import {WebViewQuillViewer} from 'react-native-webview-quilljs';
import Label from 'shared/component/Row/Label';

export default class Viewer extends Component {
  static propTypes = {
    //from connect
    header: PropTypes.string,
    content: PropTypes.object.isRequired,
  };

  handlerStartSetContent = () => {
    this.webViewQuillEditor.getDelta();
  };
  onLoadViewer = () => {
    this.loadEditor = true;
  };
  componentDidUpdate = () => {
    const {content} = this.props;

    if (this.loadEditor) {
      this.webViewQuillViewer.sendContentToViewer(content);
    }
  };
  refViewer = component => (this.webViewQuillViewer = component);
  render() {
    const {header, content} = this.props;
    return (
      <View style={styles.container}>
        <View style={styles.label}>{header && <Label text={header} />}</View>
        <View style={styles.container}>
          <WebViewQuillViewer
            ref={this.refViewer}
            contentToDisplay={content}
            onLoad={this.onLoadViewer}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  label: {
    marginLeft: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});
