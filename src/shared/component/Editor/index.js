import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {
  View,
  StyleSheet,
  Modal,
  Button,
  TouchableOpacity,
  Text,
} from 'react-native';
import {WebViewQuillEditor} from 'react-native-webview-quilljs';

export default class Editor extends Component {
  static propTypes = {
    setDataEditor: PropTypes.func.isRequired,
    titleButtonChange: PropTypes.string,
    titleButtonModal: PropTypes.string,
  };

  state = {
    isVisibleModal: false,
  };

  handlerOpenModal = () => {
    this.setState ({isVisibleModal: true});
  };

  handlerCloseModal = () => {
    this.setState ({isVisibleModal: false});
  };

  setContent = ({delta}) => {
    const {setDataEditor} = this.props;
    setDataEditor (delta);
    this.handlerCloseModal ();
  };

  handlerStartSetContent = () => {
    this.webViewQuillEditor.getDelta ();
  };

  refEditor = component => (this.webViewQuillEditor = component);

  render () {
    const {isVisibleModal} = this.state;
    const {titleButtonChange, titleButtonModal, content} = this.props;

    return (
      <View style={styles.container}>
        <Modal
          animationType="slide"
          transparent={true}
          visible={isVisibleModal}
          onRequestClose={this.handlerCloseModal}
        >
          <View style={styles.modalBackgroundStyle}>
            <View style={styles.innerContainerTransparentStyle}>
              <View style={styles.list}>
                <WebViewQuillEditor
                  ref={this.refEditor}
                  contentToDisplay={content}
                  getDeltaCallback={this.setContent}
                  onDeltaChangeCallback={this.handlerChangeContent}
                  style={styles.container}
                />
              </View>

              <View style={styles.buttonContainer}>
                <View style={styles.button}>
                  <Button title="close" onPress={this.handlerCloseModal} />
                </View>
                <View style={styles.button}>
                  <Button
                    title={titleButtonChange || 'save'}
                    onPress={this.handlerStartSetContent}
                  />
                </View>
              </View>
            </View>
          </View>
        </Modal>

        <TouchableOpacity
          onPress={this.handlerOpenModal}
          style={styles.buttonName}
        >
          <Text style={styles.textButtonName}>
            {titleButtonModal || 'open'}
          </Text>
        </TouchableOpacity>

      </View>
    );
  }
}

const styles = StyleSheet.create ({
  container: {
    flex: 1,
  },
  modalBackgroundStyle: {
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    alignItems: 'center',
    justifyContent: 'center',
  },
  innerContainerTransparentStyle: {
    display: 'flex',
    justifyContent: 'space-between',
    backgroundColor: '#ffffff',
    // padding: 10,
    width: '100%',
    height: '100%',
  },
  list: {height: '80%'},
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
    marginBottom: 10,
  },
  buttonName: {
    backgroundColor: '#841584',
    alignItems: 'center',
  },

  textButtonName: {
    fontSize: 18,

    color: '#ffffff',
    paddingLeft: 50,
    paddingRight: 50,
    paddingTop: 5,
    paddingBottom: 5,
  },
  button: {
    marginLeft: 10,
    padding: 5,
    width: 150,
  },
});
