import React from 'react';
import PropTypes from 'prop-types';
import {StyleSheet} from 'react-native';
import {Avatar} from 'react-native-elements';

const SaveButton = ({onPress}) => (
  <Avatar
    containerStyle={styles.avatarContainerStyle}
    titleStyle={styles.avatar}
    large
    rounded
    title="save"
    icon={{name: 'save', type: 'save'}}
    onPress={onPress}
    activeOpacity={0.7}
  />
);

SaveButton.propTypes = {
  onPress: PropTypes.func,
};

const styles = StyleSheet.create ({
  avatar: {
    fontSize: 23,
  },
  avatarContainerStyle: {
    backgroundColor: '#00ff40',
    margin: 15,
    elevation: 12,
  },
});

export default SaveButton;
