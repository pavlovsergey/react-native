import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Error from 'shared/component/Error';
import Loader from 'shared/component/Loader';
import {connect} from 'react-redux';
import {checkRootCategory} from 'shared/utils/category/CRUD-categoryTree';

export const loader = (
  mapStateToProps,
  mapDispatchToProps
) => OriginComponent => {
  return connect (mapStateToProps, mapDispatchToProps) (
    class extends Component {
      static propTypes = {
        //from connect
        communication: PropTypes.object.isRequired,
        load: PropTypes.func.isRequired,
        param: PropTypes.object,
      };
      static defaultProps = {
        param: {},
        communication: {},
      };
      componentDidMount = () => {
        this.runLoadData ();
      };
      componentDidUpdate = () => {
        this.runLoadData ();
      };
      getObjectParam = () => {
        const {param: {id = false}} = this.props;

        return checkRootCategory (id)
          ? this.props.communication[id]
          : this.props.communication;
      };

      runLoadData = () => {
        const {load, param: {id = false}, param} = this.props;
        const {isErrorLoad, isLoading, isLoad} = this.getObjectParam ();
        if (!isLoad && !isLoading && !isErrorLoad) return load (param);
      };
      render () {
        const {
          isErrorLoad,
          isLoading,
          isLoad,
          error,
          message,
        } = this.getObjectParam ();

        if (isErrorLoad) return <Error error={error} />;
        if (isLoading) return <Loader label={message} />;
        if (isLoad) return <OriginComponent {...this.props} />;
        return null;
      }
    }
  );
};
