import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';

const mapStateToProps = state => {
  return {
    authRole: state.auth.role,
  };
};

@connect (mapStateToProps)
export default class Permission extends Component {
  static propTypes = {
    role: PropTypes.array.isRequired,
    authRole: PropTypes.string.isRequired,
  };

  render () {
    const {role, children, authRole} = this.props;
    if (role.indexOf (authRole) !== -1) return children;
    return null;
  }
}
