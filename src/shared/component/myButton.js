import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Button} from 'react-native';

class MyButton extends Component {
  static propTypes = {
    id: PropTypes.string,
    handlerOnPressButton: PropTypes.func,
    name: PropTypes.string.isRequired,
    disabled: PropTypes.bool,
  };

  handlerOnPress = () => {
    const {id, handlerOnPressButton} = this.props;
    handlerOnPressButton && handlerOnPressButton (id);
  };

  render () {
    const {name, disabled} = this.props;
    return (
      <Button
        onPress={this.handlerOnPress}
        title={name}
        color="#841584"
        accessibilityLabel={`this purple button ${name}`}
        disabled={disabled}
      />
    );
  }
}

export default MyButton;
