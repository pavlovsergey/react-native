export const setLoadingTrue = (field, id) => {
  if (id) {
    const targetField = field[id];
    return {...field, [id]: setLoadingTrue (targetField)};
  }
  return {
    ...field,
    isLoading: true,
  };
};

export const setLoadTrue = (field, id) => {
  if (id) {
    const targetField = field[id];
    return {...field, [id]: setLoadTrue (targetField)};
  }
  return {
    ...field,
    isLoading: false,
    isLoad: true,
  };
};

export const setLoadFail = (field, {message, title, param}) => {
  if (param) {
    const targetField = field[param];
    return {...field, [param]: setLoadFail (targetField, {message, title})};
  }

  return {
    ...field,
    isLoading: false,
    isErrorLoad: true,
    error: {message, name: title},
  };
};
