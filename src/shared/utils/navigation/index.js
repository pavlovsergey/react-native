export const setNavigationName = navigation => {
  if (!navigation.isFocused) return null;
  return {
    title: `${navigation.state.params.title}`,
  };
};
