import categoryTreeBuilder from './categoryTreeBuilder';

export const ROOT_CATEGORY = 'null';
export const NEW_CATEGORY = 'null';

const initTree = {
  category: {
    [ROOT_CATEGORY]: {
      _id: null,
      title: 'All category',
      parentId: null,
    },
  },
  categoryСhildren: {[ROOT_CATEGORY]: [ROOT_CATEGORY]},
};

export const createTree = (categoryArray = []) => {
  const categoryTree = new categoryTreeBuilder (initTree);
  categoryArray.forEach (item => {
    categoryTree.addCategoryToCategoryObject (item);
    categoryTree.addСhildrenToCategoryСhildren (item);
  });
  return {
    category: categoryTree.category,
    categoryСhildren: categoryTree.categoryСhildren,
  };
};

export const addCategory = (category, categoryСhildren, newCategory) => {
  const newTree = new categoryTreeBuilder ({category, categoryСhildren});
  newTree.addCategoryToCategoryObject (newCategory);
  newTree.addСhildrenToCategoryСhildren (newCategory);
  return {
    category: newTree.category,
    categoryСhildren: newTree.categoryСhildren,
  };
};

export const deleteCategory = (
  currentCategoryTree,
  idDeletedCategory,
  chooseCategory
) => {
  const newTree = new categoryTreeBuilder (currentCategoryTree);

  newTree.categoryСhildren[idDeletedCategory] &&
    newTree.categoryСhildren[idDeletedCategory].forEach (item => {
      if (checkRootCategory (item)) {
        return newTree.deleteCategoryFromCategoryObject (item);
      }
      return;
    });

  newTree.clearCategoryСhildren (idDeletedCategory);
  newTree.deleteCategoryFromCategoryObject (idDeletedCategory);
  return {
    ...newTree.getCategoryTree (),
    chooseCategory: chooseCategory === idDeletedCategory
      ? getFirstCategory (newTree.categoryСhildren)
      : chooseCategory,
  };
};

export const updateCategory = (currentCategoryTree, newCategory) => {
  const {parentId, _id: id} = newCategory;
  const newTree = new categoryTreeBuilder (currentCategoryTree);

  if (newTree.isParentId (id, parentId)) {
    newTree.addCategoryToCategoryObject (newCategory);
    return newTree.getCategoryTree ();
  }

  newTree.clearCategoryСhildren (id);
  newTree.addСhildrenToCategoryСhildren (newCategory);
  newTree.addCategoryToCategoryObject (newCategory);

  return newTree.getCategoryTree ();
};

export const getDataFlatList = (currentCategoryTree, isRootCategory) => {
  const newTree = new categoryTreeBuilder (currentCategoryTree);
  const chooseCategory = newTree.getChooseCategory ();
  if (chooseCategory) {
    return newTree.getDataForList (isRootCategory, chooseCategory);
  }
  return [];
};

export const goToCategory = (openCategory, targetId) => {
  return openCategory.slice (0, openCategory.indexOf (targetId) + 1);
};

export const getDataForChooseParentId = (
  currentCategoryTree,
  chooseCategory
) => {
  const newTree = new categoryTreeBuilder (currentCategoryTree);
  newTree.goToParentCategoryInOpenCategory ();

  if (chooseCategory === NEW_CATEGORY) {
    return {
      ...newTree.getCategoryTree (),
    };
  }

  newTree.clearCategoryСhildren (chooseCategory);
  return {
    ...newTree.getCategoryTree (),
  };
};

export const getDataForChooseParentCategory = (category, chooseCategory) => {
  const newTree = new categoryTreeBuilder ({category});
  if (chooseCategory === NEW_CATEGORY) {
    return {
      parentId: ROOT_CATEGORY,
      valueName: 'New Category',
    };
  }

  if (chooseCategory !== NEW_CATEGORY) {
    const valueName = newTree.getDataCategory (chooseCategory).title;
    return {
      parentId: newTree.getParentCategory (chooseCategory),
      valueName: valueName,
    };
  }
  return {
    parentId: '',
    valueName: '',
  };
};
export const checkRootCategory = id => {
  if (id === ROOT_CATEGORY) return null;
  return id;
};

export const getFirstCategory = categoryСhildren => {
  if (!categoryСhildren[ROOT_CATEGORY][1]) return null;
  return categoryСhildren[ROOT_CATEGORY][1];
};
