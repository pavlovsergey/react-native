import {ROOT_CATEGORY} from './CRUD-categoryTree';
export default class categoryTreeBuilder {
  constructor({category = {}, openCategory = [], categoryСhildren = {}}) {
    this.category = category;
    this.openCategory = openCategory;
    this.categoryСhildren = categoryСhildren;
  }
  /**
   * methods for manipulation with CategoryObject Object
   */
  addCategoryToCategoryObject = category => {
    this.category[category._id] = category;
  };

  deleteCategoryFromCategoryObject = id => {
    delete this.category[id];
  };

  isParentId = (id, parentId) => {
    if (this.checkIsRootCategory (parentId)) parentId = ROOT_CATEGORY;
    if (this.category[id].parentId === parentId) return true;
    return false;
  };

  getParentCategory = id => {
    const сategory = this.getDataCategory (id);
    if (!сategory) return;
    if (!сategory.parentId) return ROOT_CATEGORY;
    return сategory.parentId;
  };

  getDataCategory = id => this.category[id];

  /**
   * methods for manipulation with CategoryСhildren Object
   */
  addСhildrenToCategoryСhildren = ({parentId, _id}) => {
    if (this.checkIsRootCategory (parentId)) parentId = ROOT_CATEGORY;

    if (this.isChildInCategoryСhildren (parentId)) {
      this.getСhildrenCategory (parentId).push (_id);
      return;
    }
    this.categoryСhildren[parentId] = [_id];
  };

  deleteCategoryFromCategoryСhildren = id => {
    const idParent = this.getParentCategory (id);
    const parentChildren = this.getСhildrenCategory (idParent);
    this.categoryСhildren[idParent] = parentChildren.filter (item => {
      if (id === item) return false;
      return true;
    });
    if (!this.isChildInCategoryСhildren (idParent)) {
      delete this.categoryСhildren[idParent];
    }
  };

  clearCategoryСhildren = id => {
    this.deleteCategoryFromCategoryСhildren (id);
    const idParent = this.getParentCategory (id);

    if (!this.isChildInCategoryСhildren (idParent)) {
      this.goToParentCategoryInOpenCategory ();
    }
  };

  getСhildrenCategory = id => {
    return this.categoryСhildren[id] || [];
  };

  isChildInCategoryСhildren = id => {
    if (!this.categoryСhildren[id] || this.categoryСhildren[id].length === 0) {
      return false;
    }
    return true;
  };
  /**
   * methods for manipulation with  OpenCategory Array
   */
  goToParentCategoryInOpenCategory = () => {
    if (this.openCategory.length > 1) {
      this.openCategory.pop ();
    }
  };
  /**
   *Other methods  
   */
  checkIsRootCategory = id => {
    if (id !== ROOT_CATEGORY) return false;
    return true;
  };

  getCategoryTree = () => {
    return {
      category: this.category,
      categoryСhildren: this.categoryСhildren,
      openCategory: this.openCategory,
    };
  };

  getDataForList = (isRootCategory, chooseCategory) => {
    const bodyList = [];
    this.getСhildrenCategory (chooseCategory).forEach (child => {
      if (!isRootCategory && this.checkIsRootCategory (child)) return;

      const isChild = this.checkIsRootCategory (child)
        ? false
        : this.isChildInCategoryСhildren (child);
      bodyList.push ({
        key: child,
        title: this.getDataCategory (child).title,
        isChild: isChild,
      });
    });
    return bodyList;
  };

  getChooseCategory = () => {
    lengthOpenCategory = this.openCategory.length;
    return this.openCategory[--lengthOpenCategory] || null;
  };
}
