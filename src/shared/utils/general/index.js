export const isNullId = id => {
  if (id === 'null') return null;
  return id;
};

export const addElementToArrayFromObject = (object, element, key) => {
  if (object[key]) return {...object, [key]: [...object[key], element]};
  return {...object, [key]: [element]};
};

export const arrayToMapWithDefaultValue = (currentArray, value) => {
  const resultMap = {};
  currentArray.forEach (({_id: id}) => {
    resultMap[id] = {...value};
  });

  return resultMap;
};

export const deleteElementToArrayFromObject = (object, element, key) => {
  if (object[key]) {
    object[key] = object[key].filter (item => {
      if (item._id === element) return false;
      return true;
    });
    return {...object};
  }
  return {...object};
};

export const updateElementToArrayFromObject = (
  object,
  element,
  key,
  previousKey
) => {
  if (previousKey !== key) {
    object = deleteElementToArrayFromObject (object, element._id, previousKey);
    object = addElementToArrayFromObject (object, element, key);
    return {...object};
  }

  const newArray = object[key].map (item => {
    if (item._id === element._id) {
      return element;
    }
    return item;
  });
  return {...object, [key]: newArray};
};
