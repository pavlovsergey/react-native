import React from 'react';
import {View, StyleSheet} from 'react-native';
import Root from './src/components/Root';
import FlashMessage from 'react-native-flash-message';
import {Provider} from 'react-redux';
import store from './src/store/';

export default class App extends React.Component {
  render () {
    return (
      <View style={styles.container}>
        <Provider store={store}>
          <Root />
        </Provider>
        <FlashMessage position="top" />
      </View>
    );
  }
}

const styles = StyleSheet.create ({
  container: {
    flex: 1,
  },
});
